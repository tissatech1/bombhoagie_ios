//
//  HomePage.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 1/20/21.
//

import UIKit
import SideMenu
import SDWebImage
import Alamofire


class HomePage: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource,UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate {

    @IBOutlet weak var seachview: UIView!
    @IBOutlet weak var homepagetabbar: UIView!
    @IBOutlet weak var menulistView: UITableView!
    @IBOutlet weak var categoryview: UICollectionView!
    @IBOutlet weak var cartcountLbl: UILabel!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchcloseBtn: UIButton!
    @IBOutlet weak var categoryshNameLbl: UILabel!
    @IBOutlet weak var menulistTop: NSLayoutConstraint!
    
    var categorywithdataDict = NSMutableDictionary()
    var categorywithdataArray = NSArray()
    
    var TitlesList = NSArray()
    var passedrestaurantid = Int()
    var passedcategoryid = Int()
    var passcategoryname = String()
   // var menulist = NSArray()
    
    var getdataarray = NSArray()
    var menulist = NSMutableArray()
    var Specialmenulist = NSArray()
    var whichclicked = String()

    var srhPageLength = 0
    var whichdata = String()
    var scroll = String()
    var fetchedproduct = NSArray()
    var catiddlist = Array<Int>()
    var dataKeys : Array = [String]()
    var listshow = String()
    var didselect = String()
    var pagenumber = 1
    var lazyloading = String()
    var restStatusStr = String()
    var PageCount = Int()

    var isLoading = false
    
    func loadData() {
        isLoading = false
       // ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        GetMenuList()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menulist = []
        PageCount = 1
        seachview.layer.cornerRadius = 8
        
        let viewNib = UINib(nibName: "categorywithImageCell", bundle: nil)
        categoryview.register(viewNib, forCellWithReuseIdentifier: "cell")
        
        menulistView.register(UINib(nibName: "menuCells", bundle: nil), forCellReuseIdentifier: "Cell")
        
        menulistView.layer.cornerRadius = 8
        menulistView.rowHeight = UITableView.automaticDimension
        menulistView.estimatedRowHeight = 105
       
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        homepagetabbar.layer.cornerRadius = 10
      
        
      //  self.timingLbl.isHidden = true
        GlobalClass.pageback = "no"
        lazyloading = "yes"
        listshow = "withoutcat"
        didselect = "no"
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
     
        
        let globalresto = GlobalClass.restaurantGlobalid
        
        self.passedrestaurantid = Int(globalresto) ?? 202
      
        cartcountLbl.layer.cornerRadius = 6
        cartcountLbl.layer.borderWidth = 1
        cartcountLbl.layer.borderColor = UIColor(rgb: 0xFC4355).cgColor
        cartcountLbl.text = "0"
        
        seachview.layer.cornerRadius = 8
        seachview.layer.shadowColor = UIColor.lightGray.cgColor
        seachview.layer.shadowOpacity = 1
        seachview.layer.shadowOffset = .zero
        seachview.layer.shadowRadius = 1
        searchcloseBtn.isHidden = true
        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
        
        adminlogincheck()
            
        }else{
       // GetCategoryList()
        checkCartAvl()
        gettiming()
        }
        
        
    }
    
    //MARK: Admin Login
    
    func adminlogincheck(){

        let urlString = GlobalClass.DevlopmentApi + "rest-auth/login/v1/"

        AF.request(urlString, method: .post, parameters: ["username":GlobalClass.adminusername, "password":GlobalClass.adminpassword,"restaurant_id":"1"],encoding: JSONEncoding.default, headers: nil).responseJSON {
       response in
         switch response.result {
                       case .success:
                           print(response)

                           if response.response?.statusCode == 200{
                            
                            let dict :NSDictionary = response.value! as! NSDictionary
                            // print(dict)
                            
                            let tok = dict.value(forKey: "token")
                            
                            let defaults = UserDefaults.standard
                            
                            defaults.set(tok, forKey: "adminToken")
                            self.gettiming()
                          //  self.GetCategoryList()
                          
                           }else{
                            
                            if response.response?.statusCode == 401{
                                
                                ERProgressHud.sharedInstance.hide()
                               // self.sessionAlert()
                                
                            }else if response.response?.statusCode == 500{
                                
                                ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                
                               //  print(dict.value(forKey: "msg") as! String)
                            }else{
                                
                                self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                               }
                          
                           }
                           
                           break
                       case .failure(let error):
                        
                        ERProgressHud.sharedInstance.hide()
                        print(error.localizedDescription)
                        
                        let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                        
                        
                        let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                        
                        let msgrs = "URLSessionTask failed with error: The request timed out."
                        
                        
                        if error.localizedDescription == msg {
                            
                            self.showSimpleAlert(messagess:"No internet connection")
                            
                        }else if error.localizedDescription == msgr ||  error.localizedDescription == msgrs {
                            
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                        
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                        }

                           print(error)
                       }
       }


    }
    
    
    //MARK: - CollectionView Delegate Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
     //** Number of Cells in the CollectionView */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
        return TitlesList.count
            
        }
    
    
    
    //** Create a basic CollectionView Cell */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! categorywithImageCell
        
//            cell.ImageLable.layer.borderWidth = 1
//            cell.ImageLable.layer.masksToBounds = true
//            cell.ImageLable.layer.borderColor = UIColor.black.cgColor
//            cell.ImageLable.layer.cornerRadius = cell.ImageLable.frame.height/2 //This will change with corners of image and height/2 will make this circle shape
        
//        cell.ImageLable.layer.cornerRadius = 8
//        
//            cell.ImageLable.clipsToBounds = true
        
          
        cell.imagesection.layer.cornerRadius = 8
//        cell.catouterView.layer.borderWidth = 1
//        cell.catouterView.layer.borderColor = UIColor.black.cgColor
//        
            if TitlesList.count == 0 {
                
            }else{

        let dictObj = self.TitlesList[indexPath.row] as! NSDictionary
        let category = dictObj["category"]

        cell.itemsLable.text = (category as! String)
           
            }
        
            return cell
      
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

//            let cellSize = CGSize(width: 110, height: 120)
//            return cellSize
        let dictObj = self.TitlesList[indexPath.row] as! NSDictionary
        let category = dictObj["category"]
        
                let label = UILabel(frame: .zero)
                label.text = (category as! String)
                label.frame = CGRect(x: 0, y: 0, width: label.intrinsicContentSize.width, height: 0)
        
                return CGSize(width: label.frame.width, height: 40)

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.menulist = []
        isLoading = false
        PageCount = 1
        
        lazyloading = "no"
    
        let dictObj = self.TitlesList[indexPath.item] as! NSDictionary
       
        let restaurantid = dictObj["restaurant_id"]as! Int
        
        let restid = String(restaurantid)
        let defaults = UserDefaults.standard
        
        defaults.set(restid, forKey: "clickedStoreId")
        
            let categoryid = dictObj["category_id"]as! Int
                passedcategoryid = categoryid
                passedrestaurantid = restaurantid
                let categoryname  = dictObj["category"]as! String
                passcategoryname = categoryname
        
        if restStatusStr == "open" {
        
        categoryshNameLbl.text = passcategoryname
            
        }
        
            ERProgressHud.sharedInstance.show(withTitle: "Loading...")
            listshow = "withcat"
            didselect = "yes"
            
            GetMenuList()
                
        
        
    }
    
   
    
    //MARK: - Table View Delegates And Datasource
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastData = self.menulist.count - 1
        if !isLoading && indexPath.row == lastData {
            PageCount += 1
            self.loadData()
        }
    }
    
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//
//         return dataKeys.count
//
//        }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//
//        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 30))
//
//                let label = UILabel()
//                label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
//
//        if menulist.count == 0 {
//
//        }else{
//
//            if didselect == "yes" {
//
//                label.text = passcategoryname
//
//            }else{
//
//
//
//                let dictObj = self.TitlesList[section] as! NSDictionary
//                let category = dictObj["category"]
//
//            label.text = category as? String
//            }
//        }
//
//        label.textAlignment = .center
//        label.font = .boldSystemFont(ofSize: 19)
//       // label.textColor = .orange // my custom colour
//
//        label.textColor = UIColor(rgb: 0xFC4355)// my custom colour
//
//        headerView.backgroundColor = .white
//                headerView.addSubview(label)
//
//                return headerView
//
//    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//
//            return 30
//
//        }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        if whichdata == "search" {
        return fetchedproduct.count
        }else{
        return menulist.count
        }
       
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! menuCells
        
        cell.selectionStyle = .none
       
        cell.menuinView.layer.cornerRadius = 8

            cell.dishimage.layer.borderWidth = 1
            cell.dishimage.layer.masksToBounds = true
            cell.dishimage.layer.borderColor = UIColor.black.cgColor
            cell.dishimage.layer.cornerRadius = 8
            cell.dishimage.clipsToBounds = true
            
            
           
        //    let dictObj = self.menulist[indexPath.row] as! NSDictionary


            if whichdata == "search" {

                if fetchedproduct.count == 0 {
                    
                }else{
                let dictObj = self.fetchedproduct[indexPath.row] as! NSDictionary

                print(dictObj)
                
                
                var urlStr = String()
                if dictObj["productUrl"] is NSNull || dictObj["productUrl"] == nil{

                    urlStr = ""

                }else{
                    urlStr = dictObj["productUrl"] as! String
                }


                let rupee = "$"
                
    //            let pricedata = dictObj["price"]as! Double
    //            let conprice = String(pricedata)
                
                var conprice = String()
                
                    if let pricedata = dictObj["price"] as? String {
                       let getprice =  Double(pricedata)
                        conprice = String(getprice!)
                    }else if let pricedata = dictObj["price"] as? NSNumber {
                        let getprice =  pricedata.stringValue
                        let changetype = Double(getprice)
                       //  conprice = pricedata.stringValue
                        conprice = String(changetype!)
                    }
            
                cell.dishprice.text = rupee + conprice
                
                cell.dishname.text!  = dictObj["productName"] as! String

                let url = URL(string: urlStr )


                cell.dishimage.sd_imageIndicator = SDWebImageActivityIndicator.gray
                cell.dishimage.sd_setImage(with: url) { (image, error, cache, urls) in
                    if (error != nil) {
                        // Failed to load image
                        cell.dishimage.image = UIImage(named: "noimage.png")
                    } else {
                        // Successful in loading image
                        cell.dishimage.image = image
                    }
                }

                cell.dishdiscribe.text!  = dictObj["extra"] as! String

            }
                
                }else{
                    
                    if menulist.count == 0 {
                        
                    }else{

                    let dictObj = self.menulist[indexPath.row] as! NSDictionary
                    
                    
            var urlStr = String()
            if dictObj["product_url"] is NSNull || dictObj["product_url"] == nil{

                urlStr = ""

            }else{
                urlStr = dictObj["product_url"] as! String
            }


            let rupee = "$"

             //   let pricedata = dictObj["price"]
                   
                    var conprice = String()
                    
                    if let pricedata = dictObj["price"] as? String {
                         conprice = pricedata
                    }else if let pricedata = dictObj["price"] as? NSNumber {
                        
                         conprice = pricedata.stringValue
                    }
                    
                 //   let conprice = pricedata

                    cell.dishprice.text = rupee + conprice


            cell.dishname.text!  = dictObj["product_name"] as! String
                    
            cell.dishdiscribe.text!  = dictObj["extra"] as! String

            let url = URL(string: urlStr )


            cell.dishimage.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.dishimage.sd_setImage(with: url) { (image, error, cache, urls) in
                if (error != nil) {
                    // Failed to load image
                    cell.dishimage.image = UIImage(named: "noimage.png")
                } else {
                    // Successful in loading image
                    cell.dishimage.image = image
                }
            }

                }
            }

            return cell
        }
        
     
        
   
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
       
        return UITableView.automaticDimension
     
    }
    
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    if restStatusStr == "open" {
        
        let menu = self.storyboard?.instantiateViewController(withIdentifier: "MenuDetailPage") as! MenuDetailPage
        
        whichclicked = "tab"

        
        if menulistView.indexPathForSelectedRow != nil {
        
            if whichdata == "search" {
                
                let dictObj = self.fetchedproduct[indexPath.row] as! NSDictionary
                
                let reqStr = dictObj["optional"]as! Bool
                print(reqStr)
                menu.mustrequired = reqStr
               
                let pidd = dictObj["productId"]as! String

                menu.passproductid = Int(pidd)!
                menu.passproductaName = (dictObj["productName"] as? String)!

                    if dictObj["productUrl"] is NSNull {
                        menu.paasprodyctimage = ""
                    }else{
                        menu.paasprodyctimage = (dictObj["productUrl"] as? String)!
                    }

//                let proprice = dictObj["price"] as! NSNumber
//
//                menu.passunitprice = proprice.stringValue
                
                if let pricedata = dictObj["price"] as? String {
                   let getprice =  Double(pricedata)
                    menu.passunitprice = String(getprice!)
                }else if let pricedata = dictObj["price"] as? NSNumber {
                    let getprice =  pricedata.stringValue
                    let changetype = Double(getprice)
                   //  conprice = pricedata.stringValue
                    menu.passunitprice = String(changetype!)
                }
                
                let restobj = dictObj["restaurant"] as! NSDictionary
                
                let restaurantid = restobj["restaurantId"]as! String
                
                let restid = restaurantid
                let defaults = UserDefaults.standard
                
                defaults.set(restid, forKey: "clickedStoreId")
                

            }else{
            
                let dictObj = self.menulist[indexPath.row] as! NSDictionary
                
                let reqStr = dictObj["optional"]as! Bool
                print(reqStr)
                menu.mustrequired = reqStr
                
                menu.passproductid = dictObj["product_id"] as! Int
                menu.passproductaName = (dictObj["product_name"] as? String)!
                
                if dictObj["product_url"] is NSNull {
                    menu.paasprodyctimage = ""
                }else{
                    menu.paasprodyctimage = (dictObj["product_url"] as? String)!
                }
                menu.passunitprice = (dictObj["price"] as? String)!
                
                let restaurantid = dictObj["restaurant"]as! Int
                
                let restid = String(restaurantid)
                let defaults = UserDefaults.standard
                
                defaults.set(restid, forKey: "clickedStoreId")
                
            }
        }
        
        
        
        
        self.navigationController?.pushViewController(menu, animated: true)
        
        
    }else{
        
        
    }
    
}

    

 
    //MARK: - tab bar button actions

    @IBAction func locationClicked(_ sender: UIButton) {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantListPage") as! RestaurantListPage
        self.navigationController?.pushViewController(home, animated: true)
        
    }
    
    @IBAction func categorylclicked(_ sender: UIButton) {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "CategoryController") as! CategoryController
        self.navigationController?.pushViewController(home, animated: true)
        
    }
   
    @IBAction func cartClicked(_ sender: UIButton) {
        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            showSimpleAlert(messagess: "Please login")
        }else{
        
        if restStatusStr == "open" {
        
        let csrt = self.storyboard?.instantiateViewController(withIdentifier: "CartPage") as! CartPage
        self.navigationController?.pushViewController(csrt, animated: true)
            
        }else{
            
        }
        }
    }
    
    @IBAction func orderClicked(_ sender: Any) {
        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            showSimpleAlert(messagess: "Please login")
        }else{
        
        let order = self.storyboard?.instantiateViewController(withIdentifier: "OrderPage") as! OrderPage
        self.navigationController?.pushViewController(order, animated: true)
        }
    }
    
    @IBAction func moreClicked(_ sender: Any) {
        
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
        
    }
    
    
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                   //     ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                   //     ProgressHUD.dismiss()

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(login, animated: true)
                                        
                                        
                                       
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    
}

extension HomePage: SideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
       // print("SideMenu Appearing! (animated: \(animated))")
        self.view.alpha = 0.5
    }
    
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Appeared! (animated: \(animated))")
        
        self.view.alpha = 0.5
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappearing! (animated: \(animated))")
        
        self.view.alpha = 1
        
       // self.performSegue(withIdentifier: "login", sender: self)
    }
    
    
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappeared! (animated: \(animated))")
        
        self.view.alpha = 1
        
      
    }
    
    
    //MARK: Webservice Call category
        
        
        func GetCategoryList(){
            
            var admintoken = String()
            let defaults = UserDefaults.standard
            let token = UserDefaults.standard.object(forKey: "Usertype")
            if token == nil {
           
                admintoken = (defaults.object(forKey: "adminToken")as? String)!
                
            }else{
                
                admintoken = (defaults.object(forKey: "custToken")as? String)!
                
            }
            
            let autho = "token \(admintoken)"
            
            let urlString = GlobalClass.DevlopmentApi+"category/?restaurant_id=\(GlobalClass.restaurantGlobalid)"
            
               
            print(" categoryurl - \(urlString)")
            
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json",
                    "Authorization": autho
                ]
          

            AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
                response in
                  switch response.result {
                                case .success:
                                    print(response)

                                    if response.response?.statusCode == 200{
                                     
                                     let dict :NSArray = response.value! as! NSArray
                                     
                                        if dict.count == 0 {
                                            self.TitlesList = []
                                            self.categoryview.reloadData()
                                            
                                        }else{
                                            
                                            self.TitlesList = dict
                                            
                                            if self.passcategoryname == "" {
                                           
                    let dictObjcat = self.TitlesList[0] as! NSDictionary
                    let categoryiddd = dictObjcat["category_id"]
                    self.passedcategoryid = categoryiddd as! Int
                    self.passcategoryname = dictObjcat["category"]as! String
                                            
                    if self.restStatusStr == "open" {
                    self.categoryshNameLbl.text = self.passcategoryname
                        
                                            }
                                                
                                            }else{
                                                
//                                                let dictObjcat = self.TitlesList[0] as! NSDictionary
//                                                let categoryiddd = dictObjcat["category_id"]
//                                                self.passedcategoryid = categoryiddd as! Int
//                                                self.passcategoryname = dictObjcat["category"]as! String
                                                                        
                                                if self.restStatusStr == "open" {
                                                self.categoryshNameLbl.text = self.passcategoryname
                                                    
                                                                        }
                                                                            
                                                                        }
                                            
                                            self.GetMenuList()
                                            
                      
                                            print("catidlist - \(self.catiddlist)")

                                            self.categoryview.reloadData()
                                            
                                        }
                                        
                                      //  ERProgressHud.sharedInstance.hide()

                                     
                                    }else{
                                        
                      if response.response?.statusCode == 401{
                                        
                        ERProgressHud.sharedInstance.hide()

                        self.SessionAlert()
                              
                                        
                                        }
                                        
                                        
                                        if response.response?.statusCode == 500{
                                            
                                            ERProgressHud.sharedInstance.hide()

                                            let dict :NSDictionary = response.value! as! NSDictionary
                                            
                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        }
                                        
                                        
                                        
                                    }
                                    
                                    break
                                case .failure(let error):
                                    ERProgressHud.sharedInstance.hide()

                                    print(error.localizedDescription)
                                    
                                    let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                    
                                    let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                    
                                    let msgrs = "URLSessionTask failed with error: The request timed out."
                                    
                                    if error.localizedDescription == msg {

                                self.showSimpleAlert(messagess:"No internet connection")

                            }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                                self.showSimpleAlert(messagess:"Slow Internet Detected")

                                    }else{
                                    
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    }

                                       print(error)
                                }
                }
                
          
                
            }
        
  
    //MARK: Webservice Call Menu
        
        
        func GetMenuList(){
            
            var admintoken = String()
            let defaults = UserDefaults.standard
            let token = UserDefaults.standard.object(forKey: "Usertype")
            if token == nil {
                admintoken = (defaults.object(forKey: "adminToken")as? String)!
            }else{
                admintoken = (defaults.object(forKey: "custToken")as? String)!
            }
            let autho = "token \(admintoken)"
            
            var urlString = String()

                urlString = GlobalClass.DevlopmentApi+"catalog/?restaurant_id=\(passedrestaurantid)&category_id=\(passedcategoryid)&status=ACTIVE&page=\(PageCount)"
                
         
            print(" category clicked menu api - \(urlString)")
            
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json",
                    "Authorization": autho
                ]
          

            AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
                response in
                  switch response.result {
                                case .success:
                                   // print(response)

                                    if response.response?.statusCode == 200{
                                        
                                        self.getdataarray = []
                                     
                                     let dict :NSDictionary = response.value! as! NSDictionary
                                     
                                        let list:NSArray = dict.value(forKey: "results") as! NSArray

                                        
                                        print(dict)
                                        
                                        
                                        if list.count == 0 {
                                           
                                            self.whichdata = "list"
                                            self.menulist = []
                                            self.menulistView.reloadData()

                                            ERProgressHud.sharedInstance.hide()
                                          
                                          
                                        self.showSimpleAlert(messagess: "No menu available in this category")
                                            
                                            
                                        }else{
                                            
                                            self.whichdata = "list"
                                            
                                            self.menulist.addObjects(from: list as! [Any])
                 
//                                            self.menulist = list as! NSMutableArray
                                          
                                            
                                            self.menulistView.reloadData()
                                            
                                            ERProgressHud.sharedInstance.hide()
                                            
                                           
                                        }
                                        
                                        

                                     
                                    }else{
                                        
                      if response.response?.statusCode == 401{
                                        
                        ERProgressHud.sharedInstance.hide()

                        self.SessionAlert()
                              
                                        
                                        }
                                        
                                        
                                        if response.response?.statusCode == 500{
                                            
                                            ERProgressHud.sharedInstance.hide()

                                            let dict :NSDictionary = response.value! as! NSDictionary
                                            
                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        }
                                        
                                        
                                        
                                    }
                                    
                                    break
                                case .failure(let error):
                                    ERProgressHud.sharedInstance.hide()

                                    print(error.localizedDescription)
                                    
                                    let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                    
                                    let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                    
                                    let msgrs = "URLSessionTask failed with error: The request timed out."
                                    
                                    if error.localizedDescription == msg {
                                        
                                self.showSimpleAlert(messagess:"No internet connection")
                                        
                            }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                        
                                self.showSimpleAlert(messagess:"Slow internet detected")
                                        
                                    }else{
                                    
                                        self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    }

                                       print(error)
                                }
                }
                
          
                
            }
    
    
    //MARK: Webservice Call timing
        
        
        func gettiming(){
            
            var admintoken = String()
            let defaults = UserDefaults.standard
            let token = UserDefaults.standard.object(forKey: "Usertype")
            if token == nil {
                admintoken = (defaults.object(forKey: "adminToken")as? String)!
            }else{
                admintoken = (defaults.object(forKey: "custToken")as? String)!
            }
            
            let autho = "token \(admintoken)"
            
            let urlString = GlobalClass.DevlopmentApi+"hour/?restaurant_id=\(GlobalClass.restaurantGlobalid)"
            
               
            print(" categoryurl - \(urlString)")
            
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json",
                    "Authorization": autho
                ]
          

            AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
                response in
                  switch response.result {
                                case .success:
                                    print(response)

                                    if response.response?.statusCode == 200{
                                        
                                        let dict :NSDictionary = response.value! as! NSDictionary
                                       
                                        let data  = dict["status"]as! String
                                        
                                        self.restStatusStr = data
                                        
                                    self.restStatusStr = "open"
                                        
                                       // data = "closed"
                                        
                                        if data == "closed" {
                                         
                                            print("restaurant is - \(data)")
                                            
                                            self.categoryshNameLbl.text = "Closed"
                                            
                                            self.menulistView.alpha = 0.5
                                         
                                            self.categoryview.alpha = 0.5
                                            
                                        }else{
                                        
                                            
                                            print("restaurant is - open")
                                        }
                                        
                                        self.GetCategoryList()
                                        
                                    }else{
                                        
                      if response.response?.statusCode == 401{
                                        
                        ERProgressHud.sharedInstance.hide()

                        self.SessionAlert()
                              
                                        
                                        }
                                        
                                        
                                        if response.response?.statusCode == 500{
                                            
                                            ERProgressHud.sharedInstance.hide()

                                            let dict :NSDictionary = response.value! as! NSDictionary
                                            
                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        }
                                        
                                        
                                        
                                    }
                                    
                                    break
                                case .failure(let error):
                                    ERProgressHud.sharedInstance.hide()

                                    print(error.localizedDescription)
                                    
                                    let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                    
                                    let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                    
                                    let msgrs = "URLSessionTask failed with error: The request timed out."
                                    
                                    if error.localizedDescription == msg {

                                self.showSimpleAlert(messagess:"No internet connection")

                            }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                                self.showSimpleAlert(messagess:"Slow Internet Detected")

                                    }else{
                                    
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    }

                                       print(error)
                                    
                                    
                                    
                                }
                }
                
          
                
            }
        
    
    //MARK: check cart Section
    
    //MARK: check cart Api
       
        func checkCartAvl()  {
           
           let defaults = UserDefaults.standard
           
           let savedUserData = defaults.object(forKey: "custToken")as? String
           
           let customerid = defaults.integer(forKey: "custId")
           let custidStr = String(customerid)
           
           let token = "Token \(savedUserData ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
               
            let urlString = GlobalClass.DevlopmentApi+"cart/?customer_id="+custidStr+"&restaurant=" + GlobalClass.restaurantGlobalid + ""
           print("Url cust avl - \(urlString)")
               
               let headers: HTTPHeaders = [
                   "Content-Type": "application/json",
                   "Authorization": token,
                   "user_id": custidStr,
                   "action": "cart"
               ]

                AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
               response in
                 switch response.result {
                               case .success:
                                   
                                 //  print(response)

                                   if response.response?.statusCode == 200{
                                     //  self.dissmiss()
                                       
                                       let dict :NSDictionary = response.value! as! NSDictionary
                                                    //  print(dict)
                                       
                                       let status = dict.value(forKey: "results")as! NSArray
                                                      print(status)
                                                        
                                           print("store available or not - \(status)")
                                       
                                       
                                                        
                   if status.count == 0 {
                       
                  //  ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                       self.createCart()
                                                           
                   }else{
                            
                               let firstobj:NSDictionary  = status.object(at: 0) as! NSDictionary

                               let getAvbcartId = firstobj["id"] as! Int
                        let storeWRTCart = firstobj["restaurant"] as! Int


                        let defaults = UserDefaults.standard

                               defaults.set(getAvbcartId, forKey: "AvlbCartId")
                               defaults.set(storeWRTCart, forKey: "storeIdWRTCart")


                                    print("avl cart id - \(getAvbcartId)")
                                    print("cart w R to store  - \(storeWRTCart)")


                    self.cartcountApi()
                       
                 //   ERProgressHud.sharedInstance.hide()

               }
                     
               }else{
                                       
                  // self.dissmiss()
                   print(response)
                   if response.response?.statusCode == 401{
                                          
                       self.SessionAlert()
                                           
                    }else if response.response?.statusCode == 500{
                                                                      
                 //  self.dissmiss()
                                                                      
                                                                       let dict :NSDictionary = response.value! as! NSDictionary
                                                                      
                                                                      self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                                                  }else{
                                                                   
                                                                   self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                                  }
               }
                                   
                                   break
                               case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {

                            self.showSimpleAlert(messagess:"No internet connection")

                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                            self.showSimpleAlert(messagess:"Slow Internet Detected")

                                }else{
                                
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    
                                }

                                   print(error)
                            }
               }
               
         
               
           }
       
       
    
    func createCart() {
        
        
        let defaults = UserDefaults.standard
        
        let savedUserData = defaults.object(forKey: "custToken")as? String
         let token = "Token \(savedUserData ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
       
        let customerid = defaults.integer(forKey: "custId")
        let custidStr = String(customerid)
       

//        let urlString = GlobalObjects.DevlopmentApi+"cart/?customer_id=\(customerid)&restaurant_id=\(passedrestaurantid)"

        let urlString = GlobalClass.DevlopmentApi+"cart/"
        
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": token,
            "user_id": custidStr,
            "action": "cart"
        ]
        
        AF.request(urlString, method: .post, parameters: ["customer_id":customerid,"restaurant":GlobalClass.restaurantGlobalid],encoding: JSONEncoding.default, headers: headers).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 201{
                                                 //   self.dissmiss()
                                                    
                                                    let resultarr :NSDictionary = response.value! as! NSDictionary
                                
                                                print(resultarr)
                                
                                
                             //   let resultarr : NSArray = dict.value(forKey: "results") as! NSArray
                                
                                if resultarr.count == 0 {
                                  
                                    self.showSimpleAlert(messagess: "Cart not created")
                                    
                                }else{
                                
                             //   let dictObj = resultarr[0] as! NSDictionary

                                                    
                                                    let getAvbcartId = resultarr["id"] as! Int
                                                                                                      let storeWRTCart = resultarr["restaurant"] as! Int
                                                                                                      
                                                                                                      let defaults = UserDefaults.standard
                                                                                                      
                                                                                                      defaults.set(getAvbcartId, forKey: "AvlbCartId")
                                                                                                      defaults.set(storeWRTCart, forKey: "storeIdWRTCart")
                                                                                                           
                                                                                                           print("avl cart id - \(getAvbcartId)")
                                                                                                           print("cart w R to store  - \(storeWRTCart)")
                                                  
                                    self.cartcountApi()
                                    
                            //    self.dissmiss()
                                  
                       //     self.performSegue(withIdentifier: "productListVC", sender: self)
                                    
                                }
                                
                            //    ERProgressHud.sharedInstance.hide()

                                
                            }else{
                             
                              if response.response?.statusCode == 500{
                                                            
                                   //                         self.dissmiss()
                                                            
                                                             let dict :NSDictionary = response.value! as! NSDictionary
                                                       
                                let message  = dict["message"]as! String
                           let substr = "duplicate key value violates unique constraint"
                                
                                if message.contains(substr) {
                                    
                                    print("I found: \(message)")
                                }else{
                                    
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    
                                }
                                                        }else{
                                                            
                                                      //      self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                           }
                            }
                            
                            break
                        case .failure(let error):
                            ERProgressHud.sharedInstance.hide()

                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                            
                            let msgrs = "URLSessionTask failed with error: The request timed out."
                            
                            if error.localizedDescription == msg {
                                
                        self.showSimpleAlert(messagess:"No internet connection")
                                
                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                
                        self.showSimpleAlert(messagess:"Slow Internet Detected")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
        }


     }
    
  
    func cartcountApi()
   {

      let defaults = UserDefaults.standard
      let customerid = defaults.integer(forKey: "custId")

        //  let customerid = 16
        
    let urlString = GlobalClass.DevlopmentGraphql


 //   let stringempty = "query{cartItemCount(token:\"\(GlobalObjects.globGraphQlToken)\", customerId :\(customerid)){\n count\n }\n}"

        let restidd = GlobalClass.restaurantGlobalid
       
        
        let stringempty = "query{cartItemCount(token:\"\(GlobalClass.globGraphQlToken)\",customerId :\(customerid),restaurantId:\(restidd)){\n count\n }\n}"
        
            AF.request(urlString, method: .post, parameters: ["query": stringempty],encoding: JSONEncoding.default, headers: nil).responseJSON {
           response in
             switch response.result {
                           case .success:
                              // print(response)

                               if response.response?.statusCode == 200{

                              let dict :NSDictionary = response.value! as! NSDictionary
                             // print(dict)
                                
                                if dict.count == 2 {

                                   // self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                    self.cartcountLbl.text = "0"
                                    
                                    let countStr = self.cartcountLbl.text
                                    let defaults = UserDefaults.standard
                                    defaults.set(countStr, forKey: "cartdatacount")
                                    
                                }else{
                                
                                
                                
                                let status = dict.value(forKey: "data")as! NSDictionary
                              print(status)


                                let newdict = status.value(forKey: "cartItemCount")as! NSDictionary

                                let num = newdict["count"] as! Int

                                self.cartcountLbl.text = String(num)

                                    let countStr = self.cartcountLbl.text
                                    let defaults = UserDefaults.standard
                                    defaults.set(countStr, forKey: "cartdatacount")
                                    
                                }
                              //  self.dissmiss()

                               }else{

                                
                                if response.response?.statusCode == 500{
                                    
                              //      ERProgressHud.sharedInstance.hide()

                              //      let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                 //   self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                }else if response.response?.statusCode == 401{
                                    ERProgressHud.sharedInstance.hide()
                                    self.SessionAlert()
                                    
                                }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                   }
                                


                               }

                               break
                           case .failure(let error):
                            
                            ERProgressHud.sharedInstance.hide()

                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                            
                            let msgrs = "URLSessionTask failed with error: The request timed out."
                            
                            if error.localizedDescription == msg {
                                
                        self.showSimpleAlert(messagess:"No internet connection")
                                
                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                
                        self.showSimpleAlert(messagess:"Slow internet detected")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
           }


        }

    
    //MARK: - Textfield Delegate

    
    func textFieldDidBeginEditing(_ textField: UITextField) {

        searchcloseBtn.isHidden = false
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == searchTF {
            self.fetchedproduct = []
            categoryshNameLbl.isHidden = true
            menulistTop.constant = 5

            srhPageLength = 0
            searchcloseBtn.isHidden = false
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
            self.menulist = []
           whichdata = "search"
        productSearchCall()
            
        textField.resignFirstResponder()
        }

        return true
    }
    
    @IBAction func searchcloseClicked(_ sender: Any) {
        self.menulist = []
        isLoading = false
        PageCount = 1
        
        categoryshNameLbl.isHidden = false
        menulistTop.constant = 50
        searchcloseBtn.isHidden = true
        searchTF.text = ""
        searchTF.placeholder = "Search our menu"
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        listshow = "withoutcat"
        self.whichdata = "list"
        self.menulist = []
        
       // GetMenuList()
        GetCategoryList()
        
    }
    
    
    
    //MARK: Webservice Call Search product  by productname
    
    func productSearchCall() {
        
        let searchdata = searchTF.text!
        let extra = ""
        let restaurantid = GlobalClass.restaurantGlobalid
       
        let page = Int(srhPageLength)

        let urlString = GlobalClass.DevlopmentGraphql
            
     //   let stringPassed = "query{productSearch(token:\"\(GlobalClass.globGraphQlToken)\", productName :\"\(searchdata)\", restaurantId : \(restaurantid) , extra : \"\(extra)\" , first:100, skip:\(page)){\n productId\n productName\n   productUrl\n  price\n extra\n  taxExempt\n category\n {\n categoryId \n category \n } \n restaurant { \n restaurantId \n address \n} } \n}"
        

        let stringPassed = "query{productSearch(token:\"\(GlobalClass.globGraphQlToken)\", productName :\"\(searchdata)\", restaurantId : \(restaurantid) , extra : \"\(extra)\" , first:100, skip:\(page)){\n productId\n productName\n   productUrl\n  price\n extra\n optional\n taxExempt\n category\n {\n categoryId \n category \n } \n restaurant { \n restaurantId \n address \n} } \n}"
        
        
       
        
        print("pro search w textUrl- \(stringPassed)")

            AF.request(urlString, method: .post, parameters: ["query": stringPassed],encoding: JSONEncoding.default, headers: nil).responseJSON {
           response in
             switch response.result {
             case .success:
               //  print(response)

                 if response.response?.statusCode == 200{
                
                let dict :NSDictionary = response.value! as! NSDictionary
                let status = dict.value(forKey: "data")as! NSDictionary
                print(status)
                    let productsearchArr :NSArray = status.value(forKey: "productSearch")as! NSArray
                    
                   
                
                    if productsearchArr.count == 0 {
                        
//                        self.categorywithdataDict = NSMutableDictionary()
//                        self.dataKeys = []
                        self.fetchedproduct = []
                        
                        self.menulist = []
                        
                        self.showSimpleAlert(messagess:"No menu found for given search")
                        self.whichdata = "search"
                        self.menulistView.reloadData()
                        ERProgressHud.sharedInstance.hide()
                    }else{
                       
                        self.whichdata = "search"
                        
                        let adddishes = NSMutableArray()

                        adddishes.addObjects(from: self.fetchedproduct as! [Any])
                      
                     //   print("pro search w text - \(adddishes)")
                        
                        self.fetchedproduct = productsearchArr
                        
                        self.menulist = []
                       
                       
                        self.menulistView.reloadData()
                        ERProgressHud.sharedInstance.hide()
                    }
                  
                 }else{
                    
                    if response.response?.statusCode == 401{
                    
                        ERProgressHud.sharedInstance.hide()
                  self.SessionAlert()
                  
                    }else if response.response?.statusCode == 500{
                        
                        ERProgressHud.sharedInstance.hide()

                        let dict :NSDictionary = response.value! as! NSDictionary
                        
                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                    }else if response.response?.statusCode == 404{
                       
                        ERProgressHud.sharedInstance.hide()
                      //  self.fetchedproduct = []
                        
                        
                    }else{
                        
                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                       }
                    
                    
                    
                 }
                 
                 break
             case .failure(let error):
                
                ERProgressHud.sharedInstance.hide()

                print(error.localizedDescription)
                
                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                
                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                
                let msgrs = "URLSessionTask failed with error: The request timed out."
                
                if error.localizedDescription == msg {
                    
            self.showSimpleAlert(messagess:"No internet connection")
                    
        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                    
            self.showSimpleAlert(messagess:"Slow internet detected")
                    
                }else{
                
                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                }

                   print(error)
            }
           }


        }
    
    
    
}
