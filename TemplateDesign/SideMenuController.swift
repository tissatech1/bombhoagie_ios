//
//  SideMenuController.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 1/25/21.
//

import UIKit
import Alamofire

class SideMenuController: UIViewController,UITableViewDataSource, UITableViewDelegate  {

    @IBOutlet weak var sidemenuView: UIView!
    @IBOutlet weak var sidetableview: UITableView!

    var lablename = NSArray()
    var imagesarr = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
           
            lablename = ["Login","About Us","Contact Us"]
            imagesarr = ["enter.png","info.png","email.png"]
            
        }else{
        
        let defaults = UserDefaults.standard
        let userlog = defaults.object(forKey: "Userlog")as! String
        if userlog == "reguser" {
        
         lablename = ["Profile","About Us","Contact Us","Logout"]
         imagesarr = ["man.png","info.png","email.png","logout.png"]
        
        }else{
        
         lablename = ["About Us","Contact Us","Logout"]
         imagesarr = ["info.png","email.png","logout.png"]
        
        }
            
        }
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)

       // sidemenuView.layer.cornerRadius = 8
        
        sidetableview.register(UINib(nibName: "SideMenuTVCell", bundle: nil), forCellReuseIdentifier: "Cell")
        
        let nc = NotificationCenter.default
        
        nc.removeObserver(self, name: Notification.Name("Sidehide"), object: self)
        
    }
    
    //MARK: - Table View Delegates And Datasource
    
  // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return lablename.count
         

    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SideMenuTVCell

        cell.selectionStyle = .none
        
        cell.cellouter.layer.cornerRadius = 8
//        cell.cellouter.layer.shadowColor = UIColor.lightGray.cgColor
//        cell.cellouter.layer.shadowOpacity = 1
//        cell.cellouter.layer.shadowOffset = .zero
//        cell.cellouter.layer.shadowRadius = 3
        
//        cell.cellouter.layer.borderWidth = 0.5
//        cell.cellouter.layer.borderColor = UIColor.gray.cgColor
        
        cell.celllable.text = lablename[indexPath.row] as? String
        cell.cellimage.image = UIImage(named: imagesarr[indexPath.row] as! String)
        

        return cell
    }
    

    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        
            return 54
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            
            if indexPath.row == 0 {
                let loginpa = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                self.navigationController?.pushViewController(loginpa, animated: true)
                
                }else if indexPath.row == 1 {
                    
                    let about = self.storyboard?.instantiateViewController(withIdentifier: "AboutusPage") as! AboutusPage
                    self.navigationController?.pushViewController(about, animated: true)
           
                 }else if indexPath.row == 2 {
                    
                    let contact = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsPage") as! ContactUsPage
                    self.navigationController?.pushViewController(contact, animated: true)
                    
                 }
            
        }else{
        
        let defaults = UserDefaults.standard
        let userlog = defaults.object(forKey: "Userlog")as! String
        
        if userlog == "reguser" {
       
        if indexPath.row == 0 {
            let profile = self.storyboard?.instantiateViewController(withIdentifier: "ProfilePage") as! ProfilePage
            self.navigationController?.pushViewController(profile, animated: true)
            
        }else if indexPath.row == 1 {
            let about = self.storyboard?.instantiateViewController(withIdentifier: "AboutusPage") as! AboutusPage
            self.navigationController?.pushViewController(about, animated: true)
            
        }else if indexPath.row == 2 {
            let contact = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsPage") as! ContactUsPage
            self.navigationController?.pushViewController(contact, animated: true)
            
        }else if indexPath.row == 3{
           // self.performSegue(withIdentifier: "login", sender: self)

            Afterlogouthomerefresh()
        }
        }else{
            
            if indexPath.row == 0 {
                let about = self.storyboard?.instantiateViewController(withIdentifier: "AboutusPage") as! AboutusPage
                self.navigationController?.pushViewController(about, animated: true)
                
            }else if indexPath.row == 1 {
                let contact = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsPage") as! ContactUsPage
                self.navigationController?.pushViewController(contact, animated: true)
                
            }else if indexPath.row == 2{
               // self.performSegue(withIdentifier: "login", sender: self)

                Afterlogouthomerefresh()
            }
            }
        }
    }
    
    func Afterlogouthomerefresh() {
       let alert = UIAlertController(title: nil, message: "Are you sure you want to logout?",         preferredStyle: UIAlertController.Style.alert)

       alert.addAction(UIAlertAction(title: "YES", style: UIAlertAction.Style.default, handler: { _ in

               UserDefaults.standard.removeObject(forKey: "AvlbCartId")
               UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
               UserDefaults.standard.removeObject(forKey: "custToken")
               UserDefaults.standard.removeObject(forKey: "custId")
           UserDefaults.standard.removeObject(forKey: "Usertype")
           UserDefaults.standard.synchronize()

           ERProgressHud.sharedInstance.show(withTitle: "Loading...")
           self.logout()

       }))
       alert.addAction(UIAlertAction(title: "NO",
                                     style: UIAlertAction.Style.default,
                                     handler: {(_: UIAlertAction!) in
                                       //Sign out action


       }))

       if presentedViewController == nil {
           self.present(alert, animated: true, completion: nil)
       } else{
           self.dismiss(animated: false) { () -> Void in
               self.present(alert, animated: true, completion: nil)
             }
       }

    //   present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black


   }
    
    func logout(){
        
        let defaults = UserDefaults.standard
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerid = defaults.integer(forKey: "custId")
        let customeridStr = String(customerid)
        print("customeridStr = \(customeridStr)")
        print("customer token = \(String(describing: admintoken))")
        print("global customer token = \(GlobalClass.customertoken)")

        
        let autho = "token \(GlobalClass.customertoken)"
        
        let urlString = GlobalClass.DevlopmentApi+"rest-auth/logout/"
        
           
        print(" categoryurl - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]
      

        AF.request(urlString, method: .post, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                                print(response)

                                if response.response?.statusCode == 200{
                                 
                               //  let dict :NSDictionary = response.value! as! NSDictionary
                                 
                                    print(response)
                                  
                                    ERProgressHud.sharedInstance.hide()

                                    let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                    self.navigationController?.pushViewController(login, animated: true)
                                    
                                 
                                }else{
                                    
                  if response.response?.statusCode == 401{
                                    
                    ERProgressHud.sharedInstance.hide()

                   // self.SessionAlert()
                          
                                    
                                    }
                                    
                                    
                                    if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                            self.showSimpleAlert(messagess:"No internet connection")
                                    
                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
   
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                       // ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor(rgb: 0xFE9300)
    }
    
    
    @IBAction func backClicked(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)

    }

}
