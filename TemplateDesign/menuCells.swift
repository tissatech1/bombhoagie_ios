//
//  menuCells.swift
//  Restaurant
//
//  Created by TISSA Technology on 11/3/20.
//

import UIKit

class menuCells: UITableViewCell {

    @IBOutlet weak var dishimage: UIImageView!
    @IBOutlet weak var dishname: UILabel!
    @IBOutlet weak var dishprice: UILabel!
    @IBOutlet weak var dishdiscribe: UILabel!
    
    @IBOutlet weak var menuinView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
