//
//  CartPage.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 1/20/21.
//

import UIKit
import Alamofire
import SDWebImage
import SideMenu

class CartPage: UIViewController,UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var butonview: UIView!
    @IBOutlet weak var carttable: UITableView!
    @IBOutlet weak var homepagetabbar: UIView!
    @IBOutlet weak var carttitleview: UIView!
    @IBOutlet var finalTotalLbl: UILabel!
    @IBOutlet weak var cartcountLbl: UILabel!

    var CartProducts = NSArray()
    var totatpro = NSString()
    var listproductId = String()
    var totalListSum  = String()
    var getsequenceId  = String()
    var cartitemIdStr  = String()
    var quantityInt  = Int()
    var cartIdStr  = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true, animated: true)
        carttable.register(UINib(nibName: "CartCell", bundle: nil), forCellReuseIdentifier: "Cell")
    
        butonview.layer.cornerRadius = 12
        carttitleview.layer.cornerRadius = 8
        
        homepagetabbar.layer.cornerRadius = 10
        cartcountLbl.layer.cornerRadius = 6
        cartcountLbl.layer.borderWidth = 1
        cartcountLbl.layer.borderColor = UIColor(rgb: 0xFC4355).cgColor
        cartcountLbl.text = "0"
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        GetCartItems()
        
        carttable.rowHeight = UITableView.automaticDimension
        carttable.estimatedRowHeight = 98
        carttable.layer.cornerRadius = 20
    }
    
    //MARK: - Table View Delegates And Datasource
    
  // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  CartProducts.count
         

    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CartCell

        cell.selectionStyle = .none
        
        cell.tableCellOuter.layer.cornerRadius = 8
        cell.tableCellOuter.layer.shadowColor = UIColor.lightGray.cgColor
        cell.tableCellOuter.layer.shadowOpacity = 1
        cell.tableCellOuter.layer.shadowOffset = .zero
        cell.tableCellOuter.layer.shadowRadius = 3
        
        cell.deletcartBtn.tag = indexPath.row
        cell.deletcartBtn.addTarget(self, action: #selector(shareBtnPressed(_:)), for: .touchUpInside)

        cell.QtyminusBtn.tag = indexPath.row
        cell.QtyminusBtn.addTarget(self, action: #selector(QuantityminusClicked(_:)), for: .touchUpInside)
        
        cell.QtyPlusBtn.tag = indexPath.row
        cell.QtyPlusBtn.addTarget(self, action: #selector(QuantityplusClicked(_:)), for: .touchUpInside)
        
        
//        cell.counterview.layer.borderWidth = 0.5
//        cell.counterview.layer.cornerRadius = 6
//        cell.counterview.layer.borderColor = UIColor(rgb: 0xFC4355).cgColor
        
//        cell.ingredienttxtLbl.layer.borderWidth = 0.5
//        cell.ingredienttxtLbl.layer.cornerRadius = 6
//        cell.ingredienttxtLbl.layer.borderColor = UIColor.lightGray.cgColor
        
         let dictObj = self.CartProducts[indexPath.row] as! NSDictionary

        cell.cartProName.text = dictObj["product_name"] as? String

        let qnt = dictObj["quantity"] as! Int
        cell.quantityLbl.text = String(qnt)


            let rupee = "$"

            let pricedata = dictObj["line_total"]as! String

//            let Pamt = Double(pricedata)
//
//            let Tamt =  Double(Pamt!) * Double(qnt)


           cell.costLbl.text = rupee + pricedata


        var urlStr = String()

        if dictObj["product_url"] is NSNull {
            urlStr = ""
        }else{

            urlStr = dictObj["product_url"] as! String

        }


        let url = URL(string: urlStr )

       cell.cartProductImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.cartProductImg.sd_setImage(with: url) { (image, error, cache, urls) in
            if (error != nil) {
                // Failed to load image
                cell.cartProductImg.image = UIImage(named: "noimage.png")
            } else {
                // Successful in loading image
                cell.cartProductImg.image = image
            }
        }


        let list:NSArray = dictObj.value(forKey: "ingredient") as! NSArray

        if list.count == 0 {

            cell.ingredientTitle.isHidden = true

        }else{
            cell.ingredientTitle.isHidden = false

            var indnamearr:[String] = []
            var indpricearr:[Double] = []
            var indpricearrforshow:[String] = []

            for (index, element1) in list.enumerated() {

                let dictObj1 = list[index] as! NSDictionary
                let ingredprice = dictObj1["line_total"]as! String
                let ingredientname = dictObj1["ingredient_name"]as! String
                let quantitystrrr = dictObj1["quantity"]as! Int
                let convertqty = Int(quantitystrrr)

                let nameshow = "\(index+1).\(ingredientname):\nPrice :$\(ingredprice)\nQty:\(convertqty)\n\n"

                indpricearrforshow.append(nameshow)
                indnamearr.append(ingredientname)
                indpricearr.append(Double(ingredprice)!)
            }

            let total = indpricearr.reduce(0, +)

            print("ingredient total - \(total)")

            let rupee = "$"

                       let pricedata = dictObj["line_total"]as! String

                       let Pamt = Double(pricedata)

                       let Tamt =  Double(Pamt!) + total


                      cell.costLbl.text = rupee + String(format: "%.2f", Tamt)

    cell.ingredientTitle.text = indpricearrforshow.map { String($0) }.joined(separator: "")

        }
        

        return cell
    }
    

    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        
            return UITableView.automaticDimension

        
    }
    
    
    @IBAction func shareBtnPressed(_ sender: UIButton) {

     //   let buttonRow = sender.tag
        
         let index = sender.tag
        
        let dictObj = self.CartProducts[index] as! NSDictionary
        
        let delpro = dictObj["product_id"] as! Int
        let delseq = dictObj["sequence_id"] as! Int
        
        self.listproductId = String(delpro)
        self.getsequenceId = String(delseq)
        
        showSimpleAlert1()
        
    
    }
    
    
    func showSimpleAlert1() {
           let alert = UIAlertController(title: nil, message: "Are you sure you want to delete?",         preferredStyle: UIAlertController.Style.alert)

           alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.default, handler: { _ in
               //Cancel Action//
           }))
           alert.addAction(UIAlertAction(title: "YES",
                                         style: UIAlertAction.Style.default,
                                         handler: {(_: UIAlertAction!) in
                                           
                                            ERProgressHud.sharedInstance.show(withTitle: "Loading...")

                                            self.DeleteCartItems()
                                            
           }))
           self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
       }
    
    
    
    //MARK: Webservice Call Search product by category
        
        
        func GetCartItems(){
            
            let defaults = UserDefaults.standard
            
          //  let admintoken = defaults.object(forKey: "adminToken")as? String
            
            let admintoken = defaults.object(forKey: "custToken")as? String
            
            let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
            
            let cartidStr = String(avlCartId)
            
            let customerid = defaults.integer(forKey: "custId")

            let customeridStr = String(customerid)
            
            let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
          //  let trimmedString = categoryStr.removingAllWhitespaces()
            
            let urlString = GlobalClass.DevlopmentApi+"cart-item/?cart_id=\(avlCartId)&status=ACTIVE&restaurant_id=\(GlobalClass.restaurantGlobalid)"
            
           

                
            print("cartproducts get url - \(urlString)")
            
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json",
                    "Authorization": autho,
                    "user_id": customeridStr,
                    "cart_id": cartidStr,
                    "action": "cart-item"
                ]

            AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
                response in
                  switch response.result {
                                case .success:
                                    print(response)

                                    if response.response?.statusCode == 200{
                                     
                                     let dict :NSDictionary = response.value! as! NSDictionary
                                     
                                        
                                           let list:NSArray = dict.value(forKey: "results") as! NSArray
                                        
                                        if list.count == 0 {
                                            
                                            ERProgressHud.sharedInstance.show(withTitle: "Loading...")

                                           self.CartProducts = []
                                            self.carttable.reloadData()

                                            
                                            let rupee = "$"
                                            let sum = "0"
                                                
                                    self.finalTotalLbl.text = rupee + sum
                                                
                                            
                                            self.totalListSum = "0"
                                            self.showSimpleAlert(messagess:"No items avaible in cart")
                                            
                                            self.cartcountLbl.text = "0"
                                            let defaults = UserDefaults.standard
                                            defaults.set("0", forKey: "cartdatacount")
                                        }else{
                                        
                                            self.CartProducts = dict.value(forKey: "results")as! NSArray
                                            
                                        let cnt = dict.value(forKey: "count") as! Int
                                        self.totatpro = String(cnt) as NSString
                                       
                                       let ttl = dict.value(forKey: "total_cost") as! String
                                            
                                            self.totalListSum = ttl
                                            
                                            let rupee = "$"
                                            self.finalTotalLbl.text = rupee + ttl
                                             
                                             print("Total Amt - \(ttl)")
                                             print("cart products list - \(self.CartProducts)")
                                            
                                            self.carttable.reloadData()
                                            
                                            self.cartcountLbl.text = self.totatpro as String
                                            let defaults = UserDefaults.standard
                                            defaults.set(self.totatpro, forKey: "cartdatacount")
                                            
                                        }
                                          
                                        ERProgressHud.sharedInstance.hide()

                                    }else{
                                        
                      if response.response?.statusCode == 401{
                                        
                        ERProgressHud.sharedInstance.hide()

                        self.SessionAlert()
                              
                                        
                                        }
                                        
                                        
                                        if response.response?.statusCode == 500{
                                            
                                            ERProgressHud.sharedInstance.hide()

                                            let dict :NSDictionary = response.value! as! NSDictionary
                                            
                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        }
                                        
                                        
                                        
                                    }
                                    
                                    break
                                case .failure(let error):
                                    ERProgressHud.sharedInstance.hide()

                                    print(error.localizedDescription)
                                    
                                    let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                    
                                    let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                    
                                    let msgrs = "URLSessionTask failed with error: The request timed out."
                                    
                                    
                                    if error.localizedDescription == msg {
                                        
                                        self.showSimpleAlert(messagess:"No internet connection")
                                        
                                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                        
                                        self.showSimpleAlert(messagess:"Slow Internet Detected")
                                                
                                            }else{
                                            
                                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                            }

                                               print(error)
                                        }
                }
                
          
                
            }
        

    
    
    //MARK: Webservice Call delete Cart
    
    func DeleteCartItems(){
        
        let defaults = UserDefaults.standard
        
       // let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)

        let customerid = defaults.integer(forKey: "custId")

        let customeridStr = String(customerid)
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
      //  let trimmedString = categoryStr.removingAllWhitespaces()
        
        let urlString = GlobalClass.DevlopmentApi+"cart-item/\(avlCartId)/?product_id=\(listproductId)&sequence_id=\(getsequenceId)"
        
       

            
        print("cartproducts get url - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "cart-item"
            ]

        AF.request(urlString, method: .delete, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                    
                                    
                                    
                                    self.GetCartItems()
                                  print("success")
                                    
                                    
                                }else{
                                    
                                     if response.response?.statusCode == 401{
                                 
                                        ERProgressHud.sharedInstance.hide()
                                        self.SessionAlert()
                                        
                                    }
                                    
                                    
                                    if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                                    self.showSimpleAlert(messagess:"Slow Internet Detected")
                                            
                                        }else{
                                        
                                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                        }

                                           print(error)
                                    }
            }
            
      
            
        }
    
    
    
    
    
    @IBAction func checkoutBtnClicked(_ sender: UIButton) {
        
        print("Total Amt - \(totalListSum)")

        let amount = Double(totalListSum);

        if amount == nil {

            self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")

        }else{

        if amount!  == 0 {



            showSimpleAlert(messagess: "No items available in the cart to place order")


        }else{

        let defaults = UserDefaults.standard

        defaults.set(totalListSum, forKey: "totalcartPrice")

        defaults.set(CartProducts, forKey: "cartarray")


            let home = self.storyboard?.instantiateViewController(withIdentifier: "ShippingPage") as! ShippingPage
            self.navigationController?.pushViewController(home, animated: true)
            
       }

        }
        
    
        
        
    }
    
    

    //MARK: - tab bar button actions

   
    @IBAction func locationClicked(_ sender: UIButton) {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantListPage") as! RestaurantListPage
        self.navigationController?.pushViewController(home, animated: true)
        
    }
    
    @IBAction func homeClikeched(_ sender: UIButton) {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "CategoryController") as! CategoryController
        self.navigationController?.pushViewController(home, animated: true)
        
    }
    
    @IBAction func orderClicked(_ sender: Any) {
        
        let order = self.storyboard?.instantiateViewController(withIdentifier: "OrderPage") as! OrderPage
        self.navigationController?.pushViewController(order, animated: true)
        
    }
    
    @IBAction func moreClicked(_ sender: Any) {
        
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
        
    }
    
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor(rgb: 0xFC4355)
    }
        
        func SessionAlert() {
            let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

          
            alert.addAction(UIAlertAction(title: "OK",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            ERProgressHud.sharedInstance.hide()
                                            //Sign out action
                                          
                                            UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                            UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                            UserDefaults.standard.removeObject(forKey: "custToken")
                                            UserDefaults.standard.removeObject(forKey: "custId")
                                        UserDefaults.standard.removeObject(forKey: "Usertype")
                                        UserDefaults.standard.synchronize()
                                            
                                            let loginpage = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                            self.navigationController?.pushViewController(loginpage, animated: true)
                                            
            }))
            self.present(alert, animated: true, completion: nil)
            alert.view.tintColor = UIColor(rgb: 0xFC4355)
        }
        
        
    //MARK: check cart Section
    
    //MARK: check cart Api
       
        func checkCartAvl()  {
           
           let defaults = UserDefaults.standard
           
           let savedUserData = defaults.object(forKey: "custToken")as? String
           
           let customerid = defaults.integer(forKey: "custId")
           let custidStr = String(customerid)
           
           let token = "Token \(savedUserData ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
               
            let urlString = GlobalClass.DevlopmentApi+"cart/?customer_id=\(custidStr)&restaurant_id=\(GlobalClass.restaurantGlobalid)"
           print("Url cust avl - \(urlString)")
               
               let headers: HTTPHeaders = [
                   "Content-Type": "application/json",
                   "Authorization": token,
                   "user_id": custidStr,
                   "action": "cart"
               ]

                AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
               response in
                 switch response.result {
                               case .success:
                                   
                                 //  print(response)

                                   if response.response?.statusCode == 200{
                                     //  self.dissmiss()
                                       
                                       let dict :NSDictionary = response.value! as! NSDictionary
                                                    //  print(dict)
                                       
                                       let status = dict.value(forKey: "results")as! NSArray
                                                      print(status)
                                                        
                                           print("store available or not - \(status)")
                                       
                                       
                                                        
                   if status.count == 0 {
                       
                  //  ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                       self.createCart()
                                                           
                   }else{
                            
                               let firstobj:NSDictionary  = status.object(at: 0) as! NSDictionary

                               let getAvbcartId = firstobj["id"] as! Int
                        let storeWRTCart = firstobj["restaurant"] as! Int


                        let defaults = UserDefaults.standard

                               defaults.set(getAvbcartId, forKey: "AvlbCartId")
                               defaults.set(storeWRTCart, forKey: "storeIdWRTCart")


                                    print("avl cart id - \(getAvbcartId)")
                                    print("cart w R to store  - \(storeWRTCart)")


                    self.GetCartItems()
                       
                 //   ERProgressHud.sharedInstance.hide()

               }
                     
               }else{
                                       
                  // self.dissmiss()
                   print(response)
                   if response.response?.statusCode == 401{
                                          
                       self.SessionAlert()
                                           
                    }else if response.response?.statusCode == 500{
                                                                      
                 //  self.dissmiss()
                                                                      
                                                                       let dict :NSDictionary = response.value! as! NSDictionary
                                                                      
                                                                      self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                                                  }else{
                                                                   
                                                                   self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                                  }
               }
                                   
                                   break
                               case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                            self.showSimpleAlert(messagess:"No internet connection")
                                    
                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
               }
               
         
               
           }
       
       
    
    func createCart() {
        
        
        let defaults = UserDefaults.standard
        
        let savedUserData = defaults.object(forKey: "custToken")as? String
         let token = "Token \(savedUserData ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
       
        let customerid = defaults.integer(forKey: "custId")
        let custidStr = String(customerid)
       

//        let urlString = GlobalObjects.DevlopmentApi+"cart/?customer_id=\(customerid)&restaurant_id=\(passedrestaurantid)"

        let urlString = GlobalClass.DevlopmentApi+"cart/"
        
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": token,
            "user_id": custidStr,
            "action": "cart"
        ]
        
        AF.request(urlString, method: .post, parameters: ["customer_id":customerid,"restaurant":GlobalClass.restaurantGlobalid],encoding: JSONEncoding.default, headers: headers).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 201{
                                                 //   self.dissmiss()
                                                    
                                                    let resultarr :NSDictionary = response.value! as! NSDictionary
                                
                                                print(resultarr)
                                
                                
                             //   let resultarr : NSArray = dict.value(forKey: "results") as! NSArray
                                
                                if resultarr.count == 0 {
                                  
                                    self.showSimpleAlert(messagess: "Cart not created")
                                    
                                }else{
                                
                             //   let dictObj = resultarr[0] as! NSDictionary

                                                    
                                                    let getAvbcartId = resultarr["id"] as! Int
                                                                                                      let storeWRTCart = resultarr["restaurant"] as! Int
                                                                                                      
                                                                                                      let defaults = UserDefaults.standard
                                                                                                      
                                                                                                      defaults.set(getAvbcartId, forKey: "AvlbCartId")
                                                                                                      defaults.set(storeWRTCart, forKey: "storeIdWRTCart")
                                                                                                           
                                                                                                           print("avl cart id - \(getAvbcartId)")
                                                                                                           print("cart w R to store  - \(storeWRTCart)")
                                                                     
                            //    self.dissmiss()
                                  
                       //     self.performSegue(withIdentifier: "productListVC", sender: self)
                                    
                                    self.GetCartItems()
                                    
                                }
                                
                            //    ERProgressHud.sharedInstance.hide()

                                
                            }else{
                             
                              if response.response?.statusCode == 500{
                                                            
                                   //                         self.dissmiss()
                                                            
                                                             let dict :NSDictionary = response.value! as! NSDictionary
                                                            
                                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                                        }else{
                                                            
                                                      //      self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                           }
                            }
                            
                            break
                        case .failure(let error):
                            ERProgressHud.sharedInstance.hide()

                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                            
                            let msgrs = "URLSessionTask failed with error: The request timed out."
                            
                            if error.localizedDescription == msg {
                                
                        self.showSimpleAlert(messagess:"No internet connection")
                                
                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                
                        self.showSimpleAlert(messagess:"Slow Internet Detected")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
        }


     }
    
    
    @IBAction func QuantityminusClicked(_ sender: UIButton) {

        let indexPath = IndexPath(row: sender.tag, section: 0)
            let cell = carttable.cellForRow(at: indexPath) as! CartCell
        
        let getqty = cell.quantityLbl.text!
        let convertqty = Int(getqty)
        
        if convertqty! > 1 {
         
        quantityInt  = convertqty! - 1
    
         let index = sender.tag
        
        let dictObj = self.CartProducts[index] as! NSDictionary
        
        let delpro = dictObj["product_id"] as! Int
        let delseq = dictObj["sequence_id"] as! Int
        let cartiddStr = dictObj["cart_id"] as! Int
        let cartitemiddStr = dictObj["cart_item_id"] as! Int
        
        self.listproductId = String(delpro)
        self.getsequenceId = String(delseq)
        self.cartIdStr = String(cartiddStr)
        self.cartitemIdStr = String(cartitemiddStr)
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        updateToCart()
            
        }else{
            
            let index = sender.tag
           
           let dictObj = self.CartProducts[index] as! NSDictionary
           
           let delpro = dictObj["product_id"] as! Int
           let delseq = dictObj["sequence_id"] as! Int
           
           self.listproductId = String(delpro)
           self.getsequenceId = String(delseq)
           
           showSimpleAlert1()
            
        }
    
    }
    
    @IBAction func QuantityplusClicked(_ sender: UIButton) {

        let indexPath = IndexPath(row: sender.tag, section: 0)
            let cell = carttable.cellForRow(at: indexPath) as! CartCell
        
        let getqty = cell.quantityLbl.text!
        let convertqty = Int(getqty)
        quantityInt  = convertqty! + 1
       
        
         let index = sender.tag
        
        let dictObj = self.CartProducts[index] as! NSDictionary
        
        let delpro = dictObj["product_id"] as! Int
        let delseq = dictObj["sequence_id"] as! Int
        let cartiddStr = dictObj["cart_id"] as! Int
        let cartitemiddStr = dictObj["cart_item_id"] as! Int
        
        self.listproductId = String(delpro)
        self.getsequenceId = String(delseq)
        self.cartIdStr = String(cartiddStr)
        self.cartitemIdStr = String(cartitemiddStr)
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        updateToCart()
        
    
    }
    
    
    
    //MARK: Webservice Call for update to cart

     
    func updateToCart() {
       
        let defaults = UserDefaults.standard
        
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        if defaults.object(forKey: "AvlbCartId") == nil {
          
            self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
           
            
        }else{
        
         let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
            let cartidStr = String(avlCartId)
            
       // let admintoken = defaults.object(forKey: "adminToken")as? String
        
        let admintoken = defaults.object(forKey: "custToken")as? String
            
            print("customer token -\(String(describing: admintoken))")
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"

        let urlString = GlobalClass.DevlopmentApi+"cart-item/\(cartitemIdStr)/"
        
 
            let metadataDict = ["id":cartitemIdStr,"product_id":listproductId, "quantity":quantityInt,"ingredient_id":0,"sequence_id":getsequenceId,"cart":cartIdStr] as [String : Any]
          
            let productarr = [metadataDict]
            print("product element Dict - \(productarr)")
            
          
            
        
            let fileUrl = URL(string: urlString)

            var request = URLRequest(url: fileUrl!)
            request.httpMethod = "PUT"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue(autho, forHTTPHeaderField: "Authorization")
            request.setValue(customeridStr, forHTTPHeaderField: "user_id")
            request.setValue(cartidStr, forHTTPHeaderField: "cart_id")
            request.setValue("cart-item", forHTTPHeaderField: "action")


            request.httpBody = try! JSONSerialization.data(withJSONObject: metadataDict)

            AF.request(request)
                .responseJSON { response in
                    // do whatever you want here
                    switch response.result {
                                            case .success:
                                                print(response)
                    
                                                if response.response?.statusCode == 200{
                    
                                                    self.GetCartItems()
                    
                                                }else{
                    
                                                    if response.response?.statusCode == 401{
                    
                                                        ERProgressHud.sharedInstance.hide()
                                                        self.SessionAlert()
                    
                                                    }else if response.response?.statusCode == 500{
                    
                                                        ERProgressHud.sharedInstance.hide()
                    
                                                        let dict :NSDictionary = response.value! as! NSDictionary
                    
                                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                                    }else{
                    
                                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                       }
                    
                                                }
                    
                                                break
                                            case .failure(let error):
                                                ERProgressHud.sharedInstance.hide()
                    
                                                print(error.localizedDescription)
                    
                                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                    
                                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                    
                                                let msgrs = "URLSessionTask failed with error: The request timed out."
                    
                                                if error.localizedDescription == msg {
                    
                                            self.showSimpleAlert(messagess:"No internet connection")
                    
                                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                    
                                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                    
                                                }else{
                    
                                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                                }
                    
                                                   print(error)
                                            }
            }
            
            
            
                
                
//            AF.request(urlString, method: .post, parameters:productarr,encoding: JSONEncoding.default, headers: headers).responseJSON {
//        response in
//          switch response.result {
//                        case .success:
//                            print(response)
//
//                            if response.response?.statusCode == 201{
//
//                                ERProgressHud.sharedInstance.hide()
//
//                             let alert = UIAlertController(title: nil, message: "Product added successfully into the cart",         preferredStyle: UIAlertController.Style.alert)
//
//
//                               alert.addAction(UIAlertAction(title: "OK",
//                                                             style: UIAlertAction.Style.default,
//                                                             handler: {(_: UIAlertAction!) in
//
//                                      self.navigationController?.popViewController(animated: true)
//
//                               }))
//                               self.present(alert, animated: true, completion: nil)
//                                alert.view.tintColor = UIColor(rgb: 0xFE9300)
//
//                            }else{
//
//                                if response.response?.statusCode == 401{
//
//                                    ERProgressHud.sharedInstance.hide()
//                                   // self.SessionAlert()
//
//                                }else if response.response?.statusCode == 500{
//
//                                    ERProgressHud.sharedInstance.hide()
//
//                                    let dict :NSDictionary = response.value! as! NSDictionary
//
//                                    self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
//                                }else{
//
//                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
//                                   }
//
//                            }
//
//                            break
//                        case .failure(let error):
//                            ERProgressHud.sharedInstance.hide()
//
//                            print(error.localizedDescription)
//
//                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
//
//                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
//
//                            let msgrs = "URLSessionTask failed with error: The request timed out."
//
//                            if error.localizedDescription == msg {
//
//                        self.showSimpleAlert(messagess:"No internet connection")
//
//                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
//
//                        self.showSimpleAlert(messagess:"Slow Internet Detected")
//
//                            }else{
//
//                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
//                            }
//
//                               print(error)
//                        }
//        }
    }

     }
    
    
    
    
}
