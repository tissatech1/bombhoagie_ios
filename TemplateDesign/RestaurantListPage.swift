//
//  RestaurantListPage.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 1/27/21.
//

import UIKit
import SideMenu
import Alamofire
import SDWebImage

class RestaurantListPage: UIViewController,UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var locationTable: UITableView!
    @IBOutlet weak var homepagetabbar: UIView!
    var fetchedItems = NSArray()
    var fetchedrestID = String()
    var fetchedAdd = String()
    var fetchedCity = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true, animated: true)
        homepagetabbar.layer.cornerRadius = 10
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        getlocationList()
        
        locationTable.register(UINib(nibName: "LocationCell", bundle: nil), forCellReuseIdentifier: "CELL")
        
//        let nc = NotificationCenter.default
//
//        nc.removeObserver(self, name: Notification.Name("Sidehide"), object: self)
//
//        nc.addObserver(self, selector: #selector(SidehideClicked), name: Notification.Name("Sidehide"), object: nil)
    }
    
    
    
//    @objc func SidehideClicked() {
//
//
//
//        self.performSegue(withIdentifier: "login", sender: self)
//
//    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

            return fetchedItems.count
       
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

      
          
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "CELL", for: indexPath) as! LocationCell
            
            cell1.selectionStyle = .none
            
        cell1.baseouter.layer.cornerRadius = 10
        cell1.outerviewupper.layer.cornerRadius = 10

        
        cell1.imagerestaurant.layer.masksToBounds = false
        cell1.imagerestaurant.layer.cornerRadius = 10
        cell1.imagerestaurant.clipsToBounds = true
        
        
            cell1.viewstoreBtn.tag = indexPath.row
            cell1.viewstoreBtn.addTarget(self, action: #selector(viewstoreBtnClicked(_:)), for: .touchUpInside)
            
        let dictObj = self.fetchedItems[indexPath.row] as! NSDictionary

        let city = (dictObj["city"] as! String)
        let state = (dictObj["state"] as! String)
        
            cell1.placeLbl.text = city + "," + state
            cell1.placename.text = (dictObj["name"] as! String)
            cell1.placeadd.text = (dictObj["address"] as! String)
            
        var urlStr = String()
        if dictObj["restaurant_url"] is NSNull {
            urlStr = ""
        }else{

            urlStr = dictObj["restaurant_url"] as! String

        }


        let url = URL(string: urlStr )

       cell1.imagerestaurant.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell1.imagerestaurant.sd_setImage(with: url) { (image, error, cache, urls) in
            if (error != nil) {
                // Failed to load image
                cell1.imagerestaurant.image = UIImage(named: "noimage.png")
            } else {
                // Successful in loading image
                cell1.imagerestaurant.image = image
            }
        }
            
            return cell1
      
        
    }
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
           
            return 167
        
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        self.performSegue(withIdentifier: "home", sender: self)
//
//
//    }
   
    @IBAction func viewstoreBtnClicked(_ sender: UIButton) {
        
        //  self.performSegue(withIdentifier: "home", sender: self)

              let dictObj = self.fetchedItems[sender.tag] as! NSDictionary
              
              let restidint = dictObj["restaurant_id"] as? NSNumber
              
              fetchedrestID = restidint!.stringValue
              
              fetchedAdd = (dictObj["address"] as! String)
              fetchedCity = (dictObj["city"] as! String)
              
              if fetchedrestID == GlobalClass.restaurantGlobalid {
                  
                self.navigationController?.popViewController(animated: false)
                
              }else{
                  
                  let alert = UIAlertController(title: nil, message: "You want to change location",         preferredStyle: UIAlertController.Style.alert)

                  alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.default, handler: { _ in
                      //Cancel Action//
                  }))
                  alert.addAction(UIAlertAction(title: "YES",
                                                style: UIAlertAction.Style.default,
                                                handler: {(_: UIAlertAction!) in
                                                  //Sign out action
                                                  ERProgressHud.sharedInstance.show(withTitle: "Loading...")

                                                   self.addidrestaurant()
                                                   
                  }))
                  self.present(alert, animated: true, completion: nil)
                  alert.view.tintColor = UIColor.black
               
              }
              
          }
    
    
    func addidrestaurant() {

        let defaults = UserDefaults.standard
        
        let admintoken = defaults.object(forKey: "adminToken")as? String
        let customerid = defaults.integer(forKey: "custId")
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
            
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                
            ]

        let urlString = GlobalClass.DevlopmentApi+"userrestaurant/"

        AF.request(urlString, method: .post, parameters: ["user":customerid ,"restaurant":fetchedrestID],encoding: JSONEncoding.default, headers: headers).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 201{
                             
                             ERProgressHud.sharedInstance.hide()

                                GlobalClass.restaurantGlobalid = self.fetchedrestID
                                
                                let defaults = UserDefaults.standard
                                
                                defaults.set(self.fetchedrestID, forKey: "clickedStoreId")
                                
                                defaults.set(self.fetchedAdd, forKey: "addresslocation")
                                defaults.set(self.fetchedCity, forKey: "citylocation")

                                let home = self.storyboard?.instantiateViewController(withIdentifier: "HomePage") as! HomePage
                                self.navigationController?.pushViewController(home, animated: true)
                             
                            }else{
                             
                               if response.response?.statusCode == 403{
                               

                                self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                 
                             }else if response.response?.statusCode == 401{
                                 
                                 ERProgressHud.sharedInstance.hide()

                                 self.showSimpleAlert(messagess: "Username or password is incorrect")
                             }else{
                                 
                                 self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                }
                             

                            }
                            
                            break
                        case .failure(let error):
                         ERProgressHud.sharedInstance.hide()
                         print(error.localizedDescription)
                         
                         let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                         
                         
                         let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                         
                         let msgrs = "URLSessionTask failed with error: The request timed out."
                         
                         if error.localizedDescription == msg {

                             self.showSimpleAlert(messagess:"No Internet Detected")

                         }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                             self.showSimpleAlert(messagess:"Slow Internet Detected")

                         }else{
                         
                             self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                        }

                            print(error)
                        }
        }


     }
    
    
    
    //MARK: - tab bar button actions

    @IBAction func homeClikeched(_ sender: UIButton) {
        

        let home = self.storyboard?.instantiateViewController(withIdentifier: "CategoryController") as! CategoryController
        self.navigationController?.pushViewController(home, animated: true)
        
        
    }
    
    @IBAction func CartClicked(_ sender: UIButton) {
        
        let cart = self.storyboard?.instantiateViewController(withIdentifier: "CartPage") as! CartPage
        self.navigationController?.pushViewController(cart, animated: true)
        
    }
    
    @IBAction func orderClicked(_ sender: Any) {
        
        let order = self.storyboard?.instantiateViewController(withIdentifier: "OrderPage") as! OrderPage
        self.navigationController?.pushViewController(order, animated: true)
        
    }
    
    @IBAction func moreClicked(_ sender: Any) {
        
      
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
        
    }
    
    
    
}

extension RestaurantListPage: SideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
       // print("SideMenu Appearing! (animated: \(animated))")
        self.view.alpha = 0.5
    }
    
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Appeared! (animated: \(animated))")
        
        self.view.alpha = 0.5
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappearing! (animated: \(animated))")
        
        self.view.alpha = 1
    }
    
    
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
      //  print("SideMenu Disappeared! (animated: \(animated))")
        
        self.view.alpha = 1
    }
    
    
  
    func getlocationList()  {
        
        let defaults = UserDefaults.standard
        
      //  let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
           
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
      //  let trimmedString = categoryStr.removingAllWhitespaces()
        
        let urlString = GlobalClass.DevlopmentApi+"restaurant/?group_name=madurai"
         
        
        print("oderitems get url - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                
                                    let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    
                                self.fetchedItems  = dict.value(forKey: "results") as! NSArray
                                     
                                    print( self.fetchedItems)
                                    
                                    if self.fetchedItems.count == 0 {
                                       
                                        ERProgressHud.sharedInstance.hide()
                                        self.showSimpleAlert(messagess:"No location available")
                                        
                                        
                                        
                                    }else{
                                        
                                        
                                        locationTable.reloadData()
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        
                                    }
                                   
                    
                                 
                                }else{
                                    
                        if response.response?.statusCode == 401{
                                    
                            ERProgressHud.sharedInstance.hide()
                        self.SessionAlert()
                          
                           }else if response.response?.statusCode == 500{
                                        
                            ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                           }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                            self.showSimpleAlert(messagess:"No internet connection")
                                    
                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let loginpage = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(loginpage, animated: true)        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    
}
