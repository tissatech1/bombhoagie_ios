//
//  OrderPage.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 1/23/21.
//

import UIKit
import Alamofire
import SDWebImage
import SideMenu


class OrderPage: UIViewController,UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var cardview: UIView!
    @IBOutlet weak var homepagetabbar: UIView!
    @IBOutlet weak var orderTable: UITableView!
    @IBOutlet weak var cartcountLbl: UILabel!

    var orderlistaraay = NSArray()
    var orderclickid = String()
    var orderresultorderid = Int()
    var currencygetandpass = String()
    var shippingmethodStr = String()
    var paymentmethodStr = String()
    var getstorenameStr = String()
    var restStatusStr = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
       
        GlobalClass.pageback = "yes"
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        gettiming()
        
        orderTable.register(UINib(nibName: "OrderCell", bundle: nil), forCellReuseIdentifier:"Cell")
        homepagetabbar.layer.cornerRadius = 8
        cardview.layer.cornerRadius = 8
      
        cartcountLbl.layer.cornerRadius = 6
        cartcountLbl.layer.borderWidth = 1
        cartcountLbl.layer.borderColor = UIColor(rgb: 0xFC4355).cgColor
        
        orderTable.layer.cornerRadius = 20
    }
    
    
    func getorderlist()  {
        
        let defaults = UserDefaults.standard
        
      //  let admintoken = defaults.object(forKey: "adminToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        let admintoken = defaults.object(forKey: "custToken")as? String
           
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
      //  let trimmedString = categoryStr.removingAllWhitespaces()
        
        let urlString = GlobalClass.DevlopmentApi+"customer-payment/?customer_id=\(customerId)&restaurant_id=\(GlobalClass.restaurantGlobalid)"
         
        
        print("oderitems get url - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "action": "customer-payment"
            ]

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                
                                    self.orderlistaraay  = response.value! as! NSArray
                                     
                                    print( self.orderlistaraay)
                                    
                                    if self.orderlistaraay.count == 0 {
                                       
                                        ERProgressHud.sharedInstance.hide()
                                        self.showSimpleAlert(messagess:"No order available")
                                        
                                        
                                        
                                    }else{
                                        
                                        
                                        orderTable.reloadData()
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        
                                    }
                                   
                    
                                 
                                }else{
                                    
                        if response.response?.statusCode == 401{
                                    
                            ERProgressHud.sharedInstance.hide()
                        self.SessionAlert()
                          
                           }else if response.response?.statusCode == 500{
                                        
                            ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                           }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                            self.showSimpleAlert(messagess:"No internet connection")
                                    
                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let loginpage = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(loginpage, animated: true)        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    
    
    

    //MARK: - Table View Delegates And Datasource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
         return 1
        
        }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
            return orderlistaraay.count
       
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! OrderCell
        
        cell.selectionStyle = .none
       
        
        cell.itemouterview.layer.cornerRadius = 6
        cell.itemouterview.layer.shadowColor = UIColor.lightGray.cgColor
        cell.itemouterview.layer.shadowOpacity = 1
        cell.itemouterview.layer.shadowOffset = .zero
        cell.itemouterview.layer.shadowRadius = 3
        
         let dictObj = self.orderlistaraay[indexPath.row] as! NSDictionary
         
         let storedata:NSDictionary = dictObj["order"]as! NSDictionary
         
         let orderno = storedata["order_id"] as! Int
        
        cell.ordernoLbl.text = String(orderno)
         
         let strdate = (dictObj["created_at"] as! String)
         let dateFormatter = DateFormatter()
                 dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
                 dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
                 let date = dateFormatter.date(from: strdate)// create   date from string

                 // change to a readable time format and change to local time zone
        if date == nil {
         
            let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                    dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
                    let date1 = dateFormatter.date(from: strdate)
            dateFormatter.dateFormat = "MMMM dd,yyyy"
            dateFormatter.timeZone = NSTimeZone.local
            let timeStamp1 = dateFormatter.string(from: date1!)
    
    
    cell.orderdateLbl.text = timeStamp1
            
        }else{
        
                 dateFormatter.dateFormat = "MMMM dd,yyyy"
                 dateFormatter.timeZone = NSTimeZone.local
                 let timeStamp = dateFormatter.string(from: date!)
         
         
         cell.orderdateLbl.text = timeStamp
        }
    //    let country = storedata["currency"] as! String
        
        
        
//        if country == "INR" {
//
//                        let rupee = "\u{20B9}"
//
//                         let uprice = dictObj["amount"] as! String
//
//                        cell.totalLbl.text =  rupee + uprice
//
//                    }else{
                        
                    let rupee = "$"
                     
                        let priceLbl = dictObj["amount"]as! String
            
                    cell.totalLbl.text =  rupee + priceLbl
                     
                //    }
        
       
        
        let urlStr = dictObj["status"] as! String
    
         if urlStr == "CONFIRMED" {
             cell.statusLbl.text = "Confirmed"
            cell.orderCancelBtn.isHidden = true
             
         }else if urlStr == "SUCCESS"{
             cell.statusLbl.text = "Success"
            cell.orderCancelBtn.isHidden = true
             
         }else if urlStr == "FAIL"{
             cell.statusLbl.text = "Failed"
            cell.orderCancelBtn.isHidden = true
             
         }else if urlStr == "READY_TO_FULFILL"{
             cell.statusLbl.text = "Ready to Fulfill"
             cell.orderCancelBtn.isHidden = true
             
         }else if urlStr == "READY_TO_PICK"{
             cell.statusLbl.text = "Ready for Pickup"
             cell.orderCancelBtn.isHidden = true
             
         }else if urlStr == "PICKED"{
             cell.statusLbl.text = "Picked"
             cell.orderCancelBtn.isHidden = true
             
         }else if urlStr == "Completed"{
             cell.statusLbl.text = "Completed"
             cell.orderCancelBtn.isHidden = true
             
         }else if urlStr == "CANCELED"{
             cell.statusLbl.text = "Cancelled"
             cell.detailShowBtn.isHidden = true
             cell.orderCancelBtn.isHidden = true
             cell.fullviewdetailsBtn.isHidden = false
         }
       
       
         
         cell.paymentLbl.text = (dictObj["payment_method"] as! String)
        
         cell.orderCancelBtn.layer.cornerRadius = 8
         
        
         cell.orderCancelBtn.tag = indexPath.row
         cell.orderCancelBtn.addTarget(self, action: #selector(orderCancelBtnClicked(_:)), for: .touchUpInside)
         
       
        
        return cell
           
        }
    
    @IBAction func orderCancelBtnClicked(_ sender: UIButton) {
    
        let index = sender.tag
       
        let dictObj = self.orderlistaraay[index] as! NSDictionary
        
        let storedata:NSDictionary = dictObj["order"]as! NSDictionary
        
        let orderno = storedata["order_id"] as! Int
        
        orderclickid = String(orderno)
        
       
        let alert = UIAlertController(title: nil, message: "Are you sure you want to cancel?",         preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action//
        }))
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        //Sign out action
                                        ERProgressHud.sharedInstance.show(withTitle: "Loading...")

                                         self.deleteorderapi()
                                         
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
        
        
    
    }
    
    func deleteorderapi()  {
        
        let defaults = UserDefaults.standard
        
      //  let admintoken = defaults.object(forKey: "adminToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        let admintoken = defaults.object(forKey: "custToken")as? String
           
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
      //  let trimmedString = categoryStr.removingAllWhitespaces()
        
        let urlString = GlobalClass.DevlopmentApi+"payment/\(orderclickid)"
        
       

            
        print("delete get url - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "order_id": orderclickid,
                "user_id": customeridStr,
                "action": "payment"
            ]

        AF.request(urlString, method: .delete, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                                print(response)

                                if response.response?.statusCode == 200{
                                
                                   
                                    ERProgressHud.sharedInstance.hide()

                                    let alert = UIAlertController(title: "Order cencel successfully", message: nil,         preferredStyle: UIAlertController.Style.alert)

                                  
                                    alert.addAction(UIAlertAction(title: "OK",
                                                                  style: UIAlertAction.Style.default,
                                                                  handler: {(_: UIAlertAction!) in
                                                                    //Sign out action
                                                                    ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                                                                getorderlist()
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    alert.view.tintColor = UIColor(rgb: 0x199A48)
                                   
                    
                                 
                                }else{
                                    
                        if response.response?.statusCode == 401{
                                    
                            ERProgressHud.sharedInstance.hide()
                        self.SessionAlert()
                          
                           }else if response.response?.statusCode == 500{
                                        
                            ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                            
                           }else if response.response?.statusCode == 400{
                            
                            ERProgressHud.sharedInstance.hide()

                            let dict :NSDictionary = response.value! as! NSDictionary
                            
                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
               }else{
                                        
                      self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                
                                       }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                            self.showSimpleAlert(messagess:"No internet connection")
                                    
                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    
    
        
   
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
       
        return 135
     
    }
    
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    let dictObj = self.orderlistaraay[indexPath.row] as! NSDictionary
    
    let storedata:NSDictionary = dictObj["order"]as! NSDictionary
    
    let storenamedetail:NSDictionary = dictObj["restaurant"]as! NSDictionary
    getstorenameStr = storenamedetail["name"]as! String
    
    self.orderresultorderid = storedata["order_id"] as! Int
    
    if dictObj["shippingmethod"] is NSNull {
        shippingmethodStr = "Restaurant Pickup"
    }else{
    
    let shippingMDict:NSDictionary = dictObj["shippingmethod"]as! NSDictionary
    
    shippingmethodStr = shippingMDict["name"]as! String
        
    }
    paymentmethodStr = dictObj["payment_method"]as! String
    
    let defaults = UserDefaults.standard
    defaults.set(storedata, forKey: "passOrderResultfromlist")
    
    
    
    
    let orderdet = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailPage") as! OrderDetailPage
    
    orderdet.orderIDGet = self.orderresultorderid
  //  view.currencyPassed = self.currencygetandpass
    orderdet.shippingmethodStrpass = self.shippingmethodStr
    orderdet.paymentmethodStrpass = self.paymentmethodStr
    orderdet.storenamepassStr = self.getstorenameStr
    
    self.navigationController?.pushViewController(orderdet, animated: true)
    
    
}
   
    
    //MARK: - tab bar button actions

   
    @IBAction func locationClicked(_ sender: UIButton) {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantListPage") as! RestaurantListPage
        self.navigationController?.pushViewController(home, animated: true)
        
    }
    
    @IBAction func homeClikeched(_ sender: UIButton) {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "CategoryController") as! CategoryController
        self.navigationController?.pushViewController(home, animated: true)
        
    }
    
    @IBAction func cartClicked(_ sender: Any) {
        
        if restStatusStr == "open" {
        
            let csrt = self.storyboard?.instantiateViewController(withIdentifier: "CartPage") as! CartPage
            self.navigationController?.pushViewController(csrt, animated: true)
        
        }else{
            
        }
        
    }
    
    @IBAction func moreClicked(_ sender: Any) {
        
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
        
    }
    
    //MARK: Webservice Call timing
        
        
        func gettiming(){
            
            var admintoken = String()
            let defaults = UserDefaults.standard
            let token = UserDefaults.standard.object(forKey: "Usertype")
            if token == nil {
                admintoken = (defaults.object(forKey: "adminToken")as? String)!
            }else{
                admintoken = (defaults.object(forKey: "custToken")as? String)!
            }
            
            let autho = "token \(admintoken)"
            
            let urlString = GlobalClass.DevlopmentApi+"hour/?restaurant_id=\(GlobalClass.restaurantGlobalid)"
            
               
            print(" categoryurl - \(urlString)")
            
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json",
                    "Authorization": autho
                ]
          

            AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
                response in
                  switch response.result {
                                case .success:
                                    print(response)

                                    if response.response?.statusCode == 200{
                                        
                                        let dict :NSDictionary = response.value! as! NSDictionary
                                       
                                        let data  = dict["status"]as! String
                                        
                                        self.restStatusStr = data
                                        
                                      //  self.restStatusStr = "open"
                                        
                                       // data = "closed"
                                        
                                        if data == "closed" {
                                         
                                            print("restaurant is - \(data)")
                                            
                                           
                                            
                                        }else{
                                        
                                            
                                            print("restaurant is - open")
                                        }
                                        
                                        self.getorderlist()
                                        self.cartcountApi()
                                    }else{
                                        
                      if response.response?.statusCode == 401{
                                        
                        ERProgressHud.sharedInstance.hide()

                        self.SessionAlert()
                              
                                        
                                        }
                                        
                                        
                                        if response.response?.statusCode == 500{
                                            
                                            ERProgressHud.sharedInstance.hide()

                                            let dict :NSDictionary = response.value! as! NSDictionary
                                            
                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        }
                                        
                                        
                                        
                                    }
                                    
                                    break
                                case .failure(let error):
                                    ERProgressHud.sharedInstance.hide()

                                    print(error.localizedDescription)
                                    
                                    let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                    
                                    let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                    
                                    let msgrs = "URLSessionTask failed with error: The request timed out."
                                    
                                    if error.localizedDescription == msg {

                                self.showSimpleAlert(messagess:"No internet connection")

                            }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                                self.showSimpleAlert(messagess:"Slow Internet Detected")

                                    }else{
                                    
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    }

                                       print(error)
                                    
                                    
                                    
                                }
                }
                
          
                
            }
    
    func cartcountApi()
   {

      let defaults = UserDefaults.standard
      let customerid = defaults.integer(forKey: "custId")

        //  let customerid = 16
        
    let urlString = GlobalClass.DevlopmentGraphql


 //   let stringempty = "query{cartItemCount(token:\"\(GlobalObjects.globGraphQlToken)\", customerId :\(customerid)){\n count\n }\n}"

        let restidd = GlobalClass.restaurantGlobalid
       
        
        let stringempty = "query{cartItemCount(token:\"\(GlobalClass.globGraphQlToken)\",customerId :\(customerid),restaurantId:\(restidd)){\n count\n }\n}"
        
            AF.request(urlString, method: .post, parameters: ["query": stringempty],encoding: JSONEncoding.default, headers: nil).responseJSON {
           response in
             switch response.result {
                           case .success:
                              // print(response)

                               if response.response?.statusCode == 200{

                              let dict :NSDictionary = response.value! as! NSDictionary
                             // print(dict)
                                
                                if dict.count == 2 {

                                   // self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                    self.cartcountLbl.text = "0"
                                    
                                    let countStr = self.cartcountLbl.text
                                    let defaults = UserDefaults.standard
                                    defaults.set(countStr, forKey: "cartdatacount")
                                    
                                }else{
                                
                                
                                
                                let status = dict.value(forKey: "data")as! NSDictionary
                              print(status)


                                let newdict = status.value(forKey: "cartItemCount")as! NSDictionary

                                let num = newdict["count"] as! Int

                                self.cartcountLbl.text = String(num)

                                    let countStr = self.cartcountLbl.text
                                    let defaults = UserDefaults.standard
                                    defaults.set(countStr, forKey: "cartdatacount")
                                    
                                }
                              //  self.dissmiss()

                               }else{

                                
                                if response.response?.statusCode == 500{
                                    
                              //      ERProgressHud.sharedInstance.hide()

                              //      let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                 //   self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                }else if response.response?.statusCode == 401{
                                    ERProgressHud.sharedInstance.hide()
                                    self.SessionAlert()
                                    
                                }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                   }
                                


                               }

                               break
                           case .failure(let error):
                            
                            ERProgressHud.sharedInstance.hide()

                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                            
                            let msgrs = "URLSessionTask failed with error: The request timed out."
                            
                            if error.localizedDescription == msg {
                                
                        self.showSimpleAlert(messagess:"No internet connection")
                                
                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                
                        self.showSimpleAlert(messagess:"Slow internet detected")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
           }


        }
    
}
