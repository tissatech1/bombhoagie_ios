//
//  PaymentPage.swift
//  TemplateDesign
//
//  Created by TISSA Technology on 1/27/21.
//

import UIKit
import Alamofire
import SDWebImage
import SideMenu
import SquareInAppPaymentsSDK

class PaymentPage: UIViewController {

    @IBOutlet weak var cardview: UIView!
    @IBOutlet weak var cardview2: UIView!
    @IBOutlet weak var tipview: UIView!
    @IBOutlet weak var instructionbox: UITextView!
    @IBOutlet weak var homepagetabbar: UIView!
    @IBOutlet weak var blurView: UIView!

    @IBOutlet weak var subtotalLbl: UILabel!
    @IBOutlet weak var taxLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var paymentNextBtn: UIButton!
    @IBOutlet weak var changeBtn: UIButton!
    @IBOutlet weak var textbaseViewTf: UITextView!
    @IBOutlet weak var cardholdernameTF: UITextField!
    @IBOutlet weak var creditcardnumberTF: UITextField!
    @IBOutlet weak var securityTF: UITextField!
    @IBOutlet weak var cardDTETF: UITextField!
    @IBOutlet weak var selectDateButton: UIButton!
    @IBOutlet weak var tipLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var cartcountLbl: UILabel!

    @IBOutlet weak var changeViewBase: UIView!
    @IBOutlet weak var title1Lbl: UILabel!
    @IBOutlet weak var title2Lbl: UILabel!
    @IBOutlet weak var per5Lbl: UILabel!
    @IBOutlet weak var per10Lbl: UILabel!
    @IBOutlet weak var rup20Lbl: UILabel!
    @IBOutlet weak var rup50Lbl: UILabel!
    @IBOutlet weak var otherLbl: UILabel!
    @IBOutlet weak var otherTfouterView: UIView!
    @IBOutlet weak var otherTf: UITextField!
    @IBOutlet weak var redio5perBtn: UIButton!
    @IBOutlet weak var redio10perBtn: UIButton!
    @IBOutlet weak var redio20RupeeBtn: UIButton!
    @IBOutlet weak var redio50RupeeBtn: UIButton!
    @IBOutlet weak var otherBtn: UIButton!
    @IBOutlet weak var changeSubmitBtn: UIButton!
    @IBOutlet weak var closeBTn: UIButton!
    
    var Shipline1 = String()
    var Shipcity = String()
    var Shipline2 = String()
    var Shippostalcode = String()
    var Shipstate = String()
    var Shipcountry = String()
    
    private var selectedDate: AVDate?
    private let calendar: AVCalendarViewController = AVCalendarViewController.calendar
    var shippingAddData = NSArray()
    var firsttimefeeData = NSArray()
    var getcartarray = NSArray()
    var parametername = String()
    var tipvalue = String()
    var otherclick = String()
    var redioclicked = String()
    
    var discoutStr = String()
    var servicefeeStr = String()
    var shippoingfeeStr = String()
    var subtotalStr = String()
    var taxStr = String()
    var totalStr = String()
    var tipStr = String()
    
    var orderresultCurrency = String()
    var orderresultamount = String()
    var orderresultorderid = Int()
    var orderresultcustomerid = Int()
    var customerPhoneNumber = String()
    var customerEmailId = String()
    var stringoOfCArdNO = String()
    var stringOfACardCVC = String()
    var stingOFCardMonth = String()
    var stringOfCardYear = String()
    var YearArr = NSArray()
    var whichbtn = String()
    var dates =  [Date]()
    var shippingmethodStr = String()
    var paymentmethodStr = String()
    var customerfullname = String()
    var orderplaced = String()
    var sourceidvalue = String()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        let defaults = UserDefaults.standard

        let shippingmethodName = defaults.object(forKey: "clickedShippingMethod")as! String
        
        print(shippingmethodName)
        
        if shippingmethodName == "PICKUP" {
            
            Shipline1 = ""
            Shipcity = ""
            Shipline2 = ""
            Shippostalcode = ""
            Shipstate = ""
            Shipcountry = ""
            
        }else{
          
            loadshippingaddress()
            
        }
        
        blurView.isHidden = true

        instructionbox.layer.borderWidth = 1
        instructionbox.layer.cornerRadius = 8
        instructionbox.layer.borderColor = UIColor.lightGray.cgColor
        
        tipview.layer.borderWidth = 1
        tipview.layer.cornerRadius = 8
        tipview.layer.borderColor = UIColor.lightGray.cgColor
        
        cardview.layer.cornerRadius = 20
        cardview2.layer.cornerRadius = 20
        homepagetabbar.layer.cornerRadius = 20
        
        parametername = "tip"
        tipvalue = "0"
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        fisrttimefeeapi()
        getcustomer()
        
        cartcountLbl.layer.cornerRadius = 6
        cartcountLbl.layer.borderWidth = 1
        cartcountLbl.layer.borderColor = UIColor(rgb: 0xFC4355).cgColor
        cartcountLbl.text = UserDefaults.standard.object(forKey: "cartdatacount") as? String
        
        
    }
    
    
    func loadshippingaddress(){
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    let urlString = GlobalClass.DevlopmentApi + "shipping/?customer_id=\(customerId)"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "shipping"
            ]

             AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                 
                                  
                                    
                                 let dict1 :NSDictionary = response.value! as! NSDictionary
                                    
                                    print("Shipping address - \(dict1)")
                                  
                                self.shippingAddData = (dict1.value(forKey:"results")as! NSArray)
                                    
                                    
                                      if self.shippingAddData.count == 0 {
                                       
                                      //  ERProgressHud.sharedInstance.hide();
                                 }else{
                                    

                                   let firstobj:NSDictionary  = self.shippingAddData.object(at: 0) as! NSDictionary
                                  
                                    
                                    self.Shipline1 = firstobj["name"] as! String
                                    
                                   let add1 = firstobj["house_number"] as! String
                                    
                                    let add2 = firstobj["address"] as! String
                                    
                                    self.Shipline2 = add1 + " " + add2
                                    
                                    self.Shipcity = firstobj["city"] as! String
                                    
                                    self.Shipcountry = firstobj["country"] as! String
                                    
                                    self.Shipstate = firstobj["state"] as! String

  
                                    self.Shippostalcode = firstobj["zip"] as! String
                                    
                                    
                                 //   ERProgressHud.sharedInstance.hide()
                                
                                 }
                                     
                                 
                                }else{
                                    
                                    if response.response?.statusCode == 401{
                                    
                                        ERProgressHud.sharedInstance.hide()
                                    
                                        self.SessionAlert()
                                        
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    private func showTheCalendar() {
        calendar.dateStyleComponents = CalendarComponentStyle(backgroundColor: UIColor(red: 89/255, green: 65/255, blue: 102/255, alpha: 1.0),
                                                              textColor: .white,
                                                              highlightColor: UIColor(red: 126/255, green: 192/255, blue: 196/255, alpha: 1.0).withAlphaComponent(0.5))
        calendar.yearStyleComponents = CalendarComponentStyle(backgroundColor: UIColor(red: 94/255, green: 200/255, blue: 252/255, alpha: 1.0),
                                                              textColor: .black, highlightColor: .white)
        calendar.monthStyleComponents = CalendarComponentStyle(backgroundColor: UIColor(red: 47/255, green: 60/255, blue: 95/255, alpha: 1.0),
                                                               textColor: UIColor(red: 126/255, green: 192/255, blue: 196/255, alpha: 1.0),
                                                               highlightColor: UIColor.white)
        calendar.subscriber = { [weak self] (date) in guard let checkedSelf = self else { return }
            if date != nil {
                checkedSelf.selectedDate = date
                let _ = Date(timeIntervalSince1970: TimeInterval(date?.doubleVal ?? 0))
              //  if let day = date?.day, let month = date?.month, let year = date?.year {
                
                if let month = date?.month, let year = date?.year {
                    let dateString = month + "/" + year
                    let datemonth = month
                    let dateyear = year
                    
                    let dateappend = month + "/" + year
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MM/yyyy"
                    let enteredDate = dateFormatter.date(from: dateappend)!
                    let endOfMonth = Calendar.current.date(byAdding: .month, value: 1, to: enteredDate)!
                    let now = Date()
                    if (endOfMonth < now) {
                        
                        let alert = UIAlertController(title: "Expired Month", message: nil,         preferredStyle: UIAlertController.Style.alert)

                      
                        alert.addAction(UIAlertAction(title: "OK",
                                                      style: UIAlertAction.Style.default,
                                                      handler: {(_: UIAlertAction!) in
                                                        //Sign out action
                                                       
                        }))
                        self!.present(alert, animated: true, completion: nil)
                        alert.view.tintColor = UIColor.black
                        
                        self!.cardDTETF.text = ""
                        self!.cardDTETF.placeholder = "MM/YYYY"
                        self!.stingOFCardMonth = ""
                        self!.stringOfCardYear = ""
                        
                    }else{
                    
                    self!.cardDTETF.text = dateString
                    if datemonth == "Jan" {
                        self!.stingOFCardMonth = "01"
                    }else if datemonth == "Feb" {
                        self!.stingOFCardMonth = "02"
                    }else if datemonth == "Mar" {
                        self!.stingOFCardMonth = "03"
                    }else if datemonth == "Apr" {
                        self!.stingOFCardMonth = "04"
                    }else if datemonth == "May" {
                        self!.stingOFCardMonth = "05"
                    }else if datemonth == "Jun" {
                        self!.stingOFCardMonth = "06"
                    }else if datemonth == "Jul" {
                        self!.stingOFCardMonth = "07"
                    }else if datemonth == "Aug" {
                        self!.stingOFCardMonth = "08"
                    }else if datemonth == "Sept" {
                        self!.stingOFCardMonth = "09"
                    }else if datemonth == "Oct" {
                        self!.stingOFCardMonth = "10"
                    }else if datemonth == "Nov" {
                        self!.stingOFCardMonth = "11"
                    }else if datemonth == "Dec" {
                        self!.stingOFCardMonth = "12"
                    }
                   // self!.stingOFCardMonth = datemonth
                    self!.stringOfCardYear = dateyear
                }
                }
            }
        }
        calendar.preSelectedDate = selectedDate
        self.present(calendar, animated: false, completion: nil)
    }
    
    
    @IBAction func paymentNextBtnClicked(_ sender: Any) {
        
//        if cardholdernameTF.text == "" || cardholdernameTF.text == nil {
//            showSimpleAlert(messagess: "Enter card holder name")
//        }else if creditcardnumberTF.text == "" || creditcardnumberTF.text == nil {
//            showSimpleAlert(messagess: "Enter card number")
//        }else if securityTF.text == "" || securityTF.text == nil {
//            showSimpleAlert(messagess: "Enter security code")
//        }else if stingOFCardMonth == "" || stingOFCardMonth.isEmpty {
//            showSimpleAlert(messagess: "Enter month")
//        }else if stringOfCardYear == "" || stringOfCardYear.isEmpty {
//            showSimpleAlert(messagess: "Enter year")
//        }else{
//
//         ERProgressHud.sharedInstance.show(withTitle: "Loading...")
//
//        orderDetailApi()
//
//            }
        
        
        didRequestPayWithCard()
        
        }
    
    
    
    
    
    private func printCurlCommand(nonce : String) {
    //    let uuid = UUID().uuidString
//        print("curl --request POST https://connect.squareup.com/v2/payments \\" +
//            "--header \"Content-Type: application/json\" \\" +
//            "--header \"Authorization: Bearer EAAAEFmC_PjW0Uutn8AW60_-ozwy3C7FwNCKWkcs0pQsFGLt2mlRrndCiQkaSDmv\" \\" +
//            "--header \"Accept: application/json\" \\" +
//            "--data \'{" +
//            "\"idempotency_key\": \"\(uuid)\"," +
//            "\"autocomplete\": true," +
//            "\"amount_money\": {" +
//            "\"amount\": 100," +
//            "\"currency\": \"USD\"}," +
//            "\"source_id\": \"\(nonce)\"" +
//            "}\'");
        sourceidvalue = nonce
        
       // orderDetailApi()
    }
    
    private var serverHostSet: Bool {
        return Constants.Square.CHARGE_SERVER_HOST != "REPLACE_ME"
    }
    
    
    @IBAction func calencerBtnCLicked(_ sender: UIButton) {
        
        showTheCalendar()
        
    }
    
   
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)

    }
    
    @IBAction func locationClicked(_ sender: UIButton) {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "RestaurantListPage") as! RestaurantListPage
        self.navigationController?.pushViewController(home, animated: true)
        
    }
    
    @IBAction func homeClikeched(_ sender: UIButton) {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "CategoryController") as! CategoryController
        self.navigationController?.pushViewController(home, animated: true)
        
    }
    
    @IBAction func CartClicked(_ sender: UIButton) {
        
        let csrt = self.storyboard?.instantiateViewController(withIdentifier: "CartPage") as! CartPage
        self.navigationController?.pushViewController(csrt, animated: true)
        
    }
    
    @IBAction func orderClicked(_ sender: Any) {
        
        let csrt = self.storyboard?.instantiateViewController(withIdentifier: "OrderPage") as! OrderPage
        self.navigationController?.pushViewController(csrt, animated: true)
        
        
    }
    
    @IBAction func moreClicked(_ sender: Any) {
        
        SideMenuManager.default.rightMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? SideMenuNavigationController
       
        present(SideMenuManager.default.rightMenuNavigationController!, animated: true, completion: nil)
        
    }
    
    
    func fisrttimefeeapi() {
        
        
         let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)

        let getshippingId = defaults.object(forKey: "clickedShippingMethodId")as! Int
        
        var subtotalStrfee = String()
        var notaxtotalstr = String()
     //  let carttotal = defaults.object(forKey: "totalcartPrice")as! String
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"

        let cartarray:NSArray = defaults.object(forKey: "cartarray")as! NSArray

        print("cart array - \(cartarray)")
        
       // var sumnotax = 0.00
        var sumsub = 0.00
           
        notaxtotalstr = "0.00"
        subtotalStrfee = "0.00"
        
        for i in 0 ..< cartarray.count  {

         //   let datagetobject:NSDictionary  = cartarray.object(at: i) as! NSDictionary
        
           
//            if (datagetobject["tax_exempt"] as! Bool == true) {
//
//                let countobj:NSDictionary  = cartarray.object(at: i) as! NSDictionary
//
//                let linetotal = countobj["line_total"] as! String
//
//
//                let qnt = Double(linetotal)
//
//
//                sumnotax = sumnotax + qnt!
//
//                notaxtotalstr = String(format: "%.2f", sumnotax)
//
//
//
//            }else{
//
               
                
//                let countobj:NSDictionary  = cartarray.object(at: i) as! NSDictionary
//
//                let linetotal = countobj["line_total"] as! String
//
//
//                let qnt = Double(linetotal)
//
//
//                sumsub = sumsub + qnt!
//
//
//                subtotalStrfee = String(format: "%.2f", sumsub)

          //  }
        
        
        }
        
        let passedcartprice = defaults.object(forKey: "totalcartPrice")as? String
        
        subtotalStrfee = passedcartprice!
    
        print("customerId - \(customerId)")
        print("getshippingId - \(getshippingId)")
        print("carttotal - \(subtotalStrfee)")
        
        print("no_tax_total - \(notaxtotalstr)")
        print("sub_total - \(subtotalStrfee)")
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "fee"
            ]

               let urlString = GlobalClass.DevlopmentApi+"fee/"

        AF.request(urlString, method: .post, parameters: ["sub_total": subtotalStrfee, "no_tax_total": notaxtotalstr,"customer_id": customerId,parametername: tipvalue,"restaurant":GlobalClass.restaurantGlobalid],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
               response in
                 switch response.result {
                               case .success:
                                print(response)

                                   if response.response?.statusCode == 200{
                                    
                                    let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    
                                    
                                     print("firstTimefee Result - \(dict)")
                                       
   
                                        self.discountLbl.text = "$\((dict["discount"] as! String))"
                                        discoutStr = (dict["discount"] as! String)
                                        
                              //  self.servicefeelbl.text = "$\((dict["service_fee"] as! String))"
                                        servicefeeStr = (dict["service_fee"] as! String)

                                        
                             //   self.shippingfeeLbl.text = "$\((dict["shipping_fee"] as! String))"
                                        shippoingfeeStr = (dict["shipping_fee"] as! String)


                            self.subtotalLbl.text = "$\((dict["sub_total"] as! String))"
                                        subtotalStr = (dict["sub_total"] as! String)

                                        
                            self.taxLbl.text = "$\((dict["tax"] as! String))"
                                        taxStr = (dict["tax"] as! String)

                                        
                            self.totalLbl.text = "$\((dict["total"] as! String))"
                                        totalStr = (dict["total"] as! String)

                                        
                        self.tipLbl.text = "$\((dict["tip"] as! String))"
                                        tipStr = (dict["tip"] as! String)
 
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    
                                   }else{
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    if response.response?.statusCode == 401{
                                        
                                        ERProgressHud.sharedInstance.hide()
                                        self.SessionAlert()
                                        
                                    }else if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                
                                    
                                   }
                                   
                                   break
                               case .failure(let error):
                                
                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                               }
               }


            }
    
    
    
    
    
    func orderDetailApi() {


       let discountdata = discoutStr

        let servicefeedata = servicefeeStr

      //  let servicefeedata = "0.00"

        let shippingfeedata = shippoingfeeStr

        let subtotaldata = subtotalStr

        let taxdata = taxStr

        let totaldata =  totalStr

        let tipdata = tipStr
        
        var commentStr = String()
        
        if textbaseViewTf.text == nil || textbaseViewTf.text! == "" {
            commentStr = ""
        }else{
            
            commentStr = textbaseViewTf.text! as String
        }
        
       print("commentStr - \(commentStr)")

      
          let  cuurency = "USD"
        

       // let cuurency = globelObjectVC.countryfixglob
        print("get currency - \(cuurency)")
        var customrtid = String()

         let defaults = UserDefaults.standard

     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)

        customrtid = String(customerId)

        let getshippingId = defaults.object(forKey: "clickedShippingMethodId")as! Int
       let carttotal = defaults.object(forKey: "totalcartPrice")as! String

        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"


        print("customerId - \(customerId)")
        print("getshippingId - \(getshippingId)")
        print("carttotal - \(carttotal)")

        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)

            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "order-detail"
            ]

               let urlString = GlobalClass.DevlopmentApi+"order-detail/"

        AF.request(urlString, method: .post, parameters: ["status": "active", "currency": cuurency,"subtotal": subtotaldata,"total": totaldata,"extra": commentStr, "customer": customrtid,"tip": tipdata,"service_fee": servicefeedata,"tax": taxdata,"discount": discountdata,"shipping_fee": shippingfeedata,"cart_id": cartidStr],encoding: JSONEncoding.default, headers: headers).responseJSON {
               response in
                 switch response.result {
                               case .success:
                                 //  print(response)

                                   if response.response?.statusCode == 200{

                                    self.orderplaced = "yes"
                                    
                                    let dict :NSDictionary = response.value! as! NSDictionary

                                     print("order detail Result - \(dict)")

                                    let defaults = UserDefaults.standard
                                    defaults.set(dict, forKey: "passOrderResult")

                                    self.orderresultCurrency = (dict["currency"] as! String)
                                    self.orderresultamount = (dict["total"] as! String)
                                    self.orderresultorderid = (dict["order_id"] as! Int)
                                    self.orderresultcustomerid = (dict["customer"] as! Int)


                                   
                                   // self.show()
                                    self.paymentApi()
                                    
                                   
                                    

                                   }else{

                                    ERProgressHud.sharedInstance.hide()

                                    if response.response?.statusCode == 401{

                                        ERProgressHud.sharedInstance.hide()
                                        self.SessionAlert()

                                    }else if response.response?.statusCode == 500{

                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary

                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }



                                   }

                                   break
                               case .failure(let error):
                                
                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                               }
               }


            }

    
    
    func getcustomer()  {
        
        let defaults = UserDefaults.standard
        
        let savedUserData = defaults.object(forKey: "custToken")as? String
        
        let customerid = defaults.integer(forKey: "custId")
        let custidStr = String(customerid)
        
        let token = "Token \(savedUserData ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
            
        let urlString = GlobalClass.DevlopmentApi+"customer/?customer_id="+custidStr+""
        print("Url cust avl - \(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": token
            ]

             AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                                
                                print(response)

                                if response.response?.statusCode == 200{
                                    ERProgressHud.sharedInstance.hide()

                                    let dict :NSDictionary = response.value! as! NSDictionary
                                                //   print(dict)
                                    
                                    let status = dict.value(forKey: "results")as! NSArray
                                                 //  print(status)

                                        print("customer detail - \(status)")

                                    let firstobj:NSDictionary  = status.object(at: 0) as! NSDictionary
                                    
                                    self.customerPhoneNumber = firstobj["phone_number"] as! String
                                    
                                    print(self.customerPhoneNumber)
                                    
                                    let customerinfo:NSDictionary = firstobj.value(forKey: "customer")as! NSDictionary
                                    
                                    
                                    self.customerEmailId = customerinfo["email"] as! String
                                    
                                //    self.customerEmailId = ""
                                    
                                    print(self.customerEmailId)
                                    
                               
                                    let firstnamesh = customerinfo["first_name"] as! String
                                    let lastnamesh = customerinfo["last_name"] as! String

                                    
                                    self.customerfullname = firstnamesh + " " + lastnamesh
                                    
                               
                                    
                                    
                      }else{
                                    
                                //    self.dissmiss()
                                    
                                    if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        
                                    }else if response.response?.statusCode == 401{
                                        ERProgressHud.sharedInstance.hide()

                                    self.SessionAlert()
                                        
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                            
                                print(response)
                                }
                                
                                break
                            case .failure(let error):
                                
                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    
    func paymentApi()  {
        
        
        let defaults = UserDefaults.standard
       
    //   let admintoken = defaults.object(forKey: "adminToken")as? String
       let admintoken = defaults.object(forKey: "custToken")as? String
       let customerId = defaults.integer(forKey: "custId")
        let  customeridStr = String(customerId)

        
       let getshippingId = defaults.object(forKey: "clickedShippingMethodId")as! Int
        let shippingmethodName = defaults.object(forKey: "clickedShippingMethod")as! String
        
        print(shippingmethodName)
        let shippingmethodID = defaults.integer(forKey: "clickedShippingMethodId")
        
        print(shippingmethodName)
        
        let storepassid = GlobalClass.restaurantGlobalid
        print(storepassid)
       
        var commentStr = String()
        
        if textbaseViewTf.text == nil || textbaseViewTf.text! == "" {
            commentStr = ""
        }else{
            
            commentStr = textbaseViewTf.text! as String
        }
        
       print("commentStr - \(commentStr)")
      
       let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"

      
        
        var metadataDict = [String : Any]()
        
        metadataDict = ["order_id":orderresultorderid, "restaurant_id":storepassid, "customer_id":orderresultcustomerid, "shippingmethod_id":shippingmethodID,"phone":customerPhoneNumber,"name":customerfullname,"special_instruction":commentStr]
        
        print(metadataDict)
        
        
        
        var cardDataDict = [String : Any]()
        
        cardDataDict = ["number":"4242424242424242", "exp_month":"02","exp_year":"2024", "cvc":"555"]
        
        print(cardDataDict)
        
        
        let billinfo:NSDictionary = defaults.object(forKey: "billingaddressDICT")as! NSDictionary

        print("billinfo dict - \(billinfo)")
        
        let cityStr = billinfo["city"] as! String
        
        let address2 = "\(billinfo["house_number"] as! String)" + "\(billinfo["address"] as! String)"
        
        let address1 = billinfo["company_name"] as! String
        
        let postalStr = billinfo["zip"] as! String
        
        let stateStr = billinfo["state"] as! String
        
        let addressDict = ["city":cityStr, "line1":address1,"line2":address2, "postal_code":postalStr,"state":stateStr]
        
        
        var addressDataDict = [String : Any]()
        
        addressDataDict = ["address":addressDict]
        print(addressDataDict)
        
        let ShipaddressDict = ["line1":Shipline1, "city":Shipcity,"line2":Shipline2, "postal_code":Shippostalcode,"state":Shipstate,"country":Shipcountry]
        
        
        var ShipaddressDataDict = [String : Any]()
        
        ShipaddressDataDict = ["address":ShipaddressDict]
       
   
       print("customerId - \(customerId)")
       print("getshippingId - \(getshippingId)")
        print("billingaddresspassed - \(addressDataDict)")
        print("billingaddresspassed - \(ShipaddressDataDict)")
       
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
        
        let orderidStrheader = String(orderresultorderid)
           
           let headers: HTTPHeaders = [
               "Content-Type": "application/json",
               "Authorization": autho,
               "order_id": orderidStrheader,
               "user_id": customeridStr,
               "cart_id": cartidStr,
               "action": "payment"
           ]

              let urlString = GlobalClass.DevlopmentApi+"payment/"

        AF.request(urlString, method: .post, parameters: ["currency": "USD", "amount": orderresultamount,"receipt_email": customerEmailId,"type": "card","card": cardDataDict,"billing_details": addressDataDict,"metadata": metadataDict,"source_id":sourceidvalue,"fulfillment":shippingmethodName,"shipping_details":ShipaddressDataDict],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
              response in
                switch response.result {
                              case .success:
                                 // print(response)

                                  if response.response?.statusCode == 200{
                                   
                                   let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    print(dict)
                                    
//                                    let shippingMDict:NSDictionary = dict["shippingmethod"]as! NSDictionary
//
//                                    shippingmethodStr = shippingMDict["name"]as! String
                           //         paymentmethodStr = dict["payment_method"]as! String
                                  
                                    print("payment Resultshow - \(dict)")
                                    ERProgressHud.sharedInstance.hide()

                                    let alert = UIAlertController(title:nil , message: "Order placed successfully",         preferredStyle: UIAlertController.Style.alert)

                                  
                                    alert.addAction(UIAlertAction(title: "OK",
                                                                  style: UIAlertAction.Style.default,
                                                                  handler: {(_: UIAlertAction!) in
                                                                    
                                                                    let home = self.storyboard?.instantiateViewController(withIdentifier: "AfterpaymentPage") as! AfterpaymentPage
                                                                    
                                                                    home.orderIDGet = self.orderresultorderid
                                                                    
                                                                    self.navigationController?.pushViewController(home, animated: true)
                                        
                                                                    
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    alert.view.tintColor = UIColor.black
                                   
                                    
                                   
                                  }else{
                                   
                                    ERProgressHud.sharedInstance.hide()

                                    if response.response?.statusCode == 400{
                                        
                                        ERProgressHud.sharedInstance.hide()
                                        self.showSimpleAlert(messagess: "Incorrect card details. Please check")
                                        
                                    }else if response.response?.statusCode == 401{
                                       
                                        ERProgressHud.sharedInstance.hide()
                                    self.SessionAlert()
                                       
                                   }else if response.response?.statusCode == 500{
                                       
                                    ERProgressHud.sharedInstance.hide()

                                       let dict :NSDictionary = response.value! as! NSDictionary
                                       
                                       self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                   }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                   }
                                   
                               
                                   
                                  }
                                  
                                  break
                              case .failure(let error):
                                
                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                              }
              }


           }
    
    
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//
//        if segue.identifier == "afterpaymentDetail" {
//            let view = segue.destination as! AfterpaymentPageViewController
//            view.orderIDGet = self.orderresultorderid
////            view.shippingmethodStrpass = self.shippingmethodStr
////            view.paymentmethodStrpass = self.paymentmethodStr
//        }
//    }
   
    
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: messagess, message: nil,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        //Sign out action
                                        ERProgressHud.sharedInstance.hide()
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor.black
    }
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        let loginpage = self.storyboard?.instantiateViewController(withIdentifier: "LoginPage") as! LoginPage
                                        self.navigationController?.pushViewController(loginpage, animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor(rgb: 0xFE9300)
    }
    
   
    @IBAction func changeBtnClicked(_ sender: Any) {
        
        self.viewshow()
        
    }
    
    @IBAction func closechangeClicked(_ sender: Any) {
        
        otherTf.resignFirstResponder()
        self.viewhide()
        
    }
    
    
    func viewshow()  {
        
        closeBTn.layer.cornerRadius = 6
        closeBTn.layer.borderWidth = 1
        closeBTn.layer.borderColor = UIColor.black.cgColor
        
        changeSubmitBtn.layer.cornerRadius = 6
        changeViewBase.layer.cornerRadius = 6
        otherTfouterView.layer.cornerRadius = 6
        
        blurView.isHidden = false
        changeViewBase.isHidden = false
     //   title1Lbl.isHidden = false
      //  title2Lbl.isHidden = false
        per5Lbl.isHidden = false
        per10Lbl.isHidden = false
        rup20Lbl.isHidden = false
        rup50Lbl.isHidden = false
        otherLbl.isHidden = false
        otherTfouterView.isHidden = false
        otherTf.isHidden = false
        redio5perBtn.isHidden = false
        redio10perBtn.isHidden = false
        redio20RupeeBtn.isHidden = false
        redio50RupeeBtn.isHidden = false
        otherBtn.isHidden = false
        changeSubmitBtn.isHidden = false
        closeBTn.isHidden = false
        otherclick = "no"
        redioclicked = "no"
        
        let imagehome = UIImage(named: "untickredio.png") as UIImage?
        self.redio5perBtn.setBackgroundImage(imagehome, for: .normal)
        
        let image1 = UIImage(named: "untickredio.png") as UIImage?
        self.redio10perBtn.setBackgroundImage(image1, for: .normal)
        
        let image2 = UIImage(named: "untickredio.png") as UIImage?
        self.redio20RupeeBtn.setBackgroundImage(image2, for: .normal)
        
        let image3 = UIImage(named: "untickredio.png") as UIImage?
        self.redio50RupeeBtn.setBackgroundImage(image3, for: .normal)
        
        let image4 = UIImage(named: "untickredio.png") as UIImage?
        self.otherBtn.setBackgroundImage(image4, for: .normal)
        
        parametername = "tip"
        tipvalue = "0"
        
    }
    
    func viewhide()  {
        
        blurView.isHidden = true
        changeViewBase.isHidden = true
      //  title1Lbl.isHidden = true
      //  title2Lbl.isHidden = true
        per5Lbl.isHidden = true
        per10Lbl.isHidden = true
        rup20Lbl.isHidden = true
        rup50Lbl.isHidden = true
        otherLbl.isHidden = true
        otherTfouterView.isHidden = true
        otherTf.isHidden = true
        redio5perBtn.isHidden = true
        redio10perBtn.isHidden = true
        redio20RupeeBtn.isHidden = true
        redio50RupeeBtn.isHidden = true
        otherBtn.isHidden = true
        changeSubmitBtn.isHidden = true
        closeBTn.isHidden = true
    }
    
    
    @IBAction func per5Btnclicked(_ sender: Any) {
        parametername = "tip"
        tipvalue = "5"
        
        let imagehome = UIImage(named: "dot.png") as UIImage?
        self.redio5perBtn.setBackgroundImage(imagehome, for: .normal)
        
        let image1 = UIImage(named: "untickredio.png") as UIImage?
        self.redio10perBtn.setBackgroundImage(image1, for: .normal)
        
        let image2 = UIImage(named: "untickredio.png") as UIImage?
        self.redio20RupeeBtn.setBackgroundImage(image2, for: .normal)
        
        let image3 = UIImage(named: "untickredio.png") as UIImage?
        self.redio50RupeeBtn.setBackgroundImage(image3, for: .normal)
        
        let image4 = UIImage(named: "untickredio.png") as UIImage?
        self.otherBtn.setBackgroundImage(image4, for: .normal)
        otherclick = "no"
        redioclicked = "yes"
        otherTf.resignFirstResponder()
    }
    
    @IBAction func per10BtnClicked(_ sender: Any) {
        parametername = "tip"
        tipvalue = "10"
        
        let imagehome = UIImage(named: "dot.png") as UIImage?
        self.redio10perBtn.setBackgroundImage(imagehome, for: .normal)
        
        let image1 = UIImage(named: "untickredio.png") as UIImage?
        self.redio5perBtn.setBackgroundImage(image1, for: .normal)
        
        let image2 = UIImage(named: "untickredio.png") as UIImage?
        self.redio20RupeeBtn.setBackgroundImage(image2, for: .normal)
        
        let image3 = UIImage(named: "untickredio.png") as UIImage?
        self.redio50RupeeBtn.setBackgroundImage(image3, for: .normal)
        
        let image4 = UIImage(named: "untickredio.png") as UIImage?
        self.otherBtn.setBackgroundImage(image4, for: .normal)
        otherclick = "no"
        redioclicked = "yes"
        otherTf.resignFirstResponder()
    }
    
    @IBAction func rupee20BtnClicked(_ sender: Any) {
//        parametername = "custom_tip"
//        tipvalue = "4"
        
        parametername = "tip"
        tipvalue = "15"
        
        let imagehome = UIImage(named: "dot.png") as UIImage?
        self.redio20RupeeBtn.setBackgroundImage(imagehome, for: .normal)
        
        let image1 = UIImage(named: "untickredio.png") as UIImage?
        self.redio5perBtn.setBackgroundImage(image1, for: .normal)
        
        let image2 = UIImage(named: "untickredio.png") as UIImage?
        self.redio10perBtn.setBackgroundImage(image2, for: .normal)
        
        let image3 = UIImage(named: "untickredio.png") as UIImage?
        self.redio50RupeeBtn.setBackgroundImage(image3, for: .normal)
        
        let image4 = UIImage(named: "untickredio.png") as UIImage?
        self.otherBtn.setBackgroundImage(image4, for: .normal)
        otherclick = "no"
        redioclicked = "yes"
        otherTf.resignFirstResponder()
    }
    
    @IBAction func rupee50BtnClicked(_ sender: Any) {
//        parametername = "custom_tip"
//        tipvalue = "6"
        
        parametername = "tip"
        tipvalue = "20"
        
        let imagehome = UIImage(named: "dot.png") as UIImage?
        self.redio50RupeeBtn.setBackgroundImage(imagehome, for: .normal)
        
        let image1 = UIImage(named: "untickredio.png") as UIImage?
        self.redio5perBtn.setBackgroundImage(image1, for: .normal)
        
        let image2 = UIImage(named: "untickredio.png") as UIImage?
        self.redio10perBtn.setBackgroundImage(image2, for: .normal)
        
        let image3 = UIImage(named: "untickredio.png") as UIImage?
        self.redio20RupeeBtn.setBackgroundImage(image3, for: .normal)
        
        let image4 = UIImage(named: "untickredio.png") as UIImage?
        self.otherBtn.setBackgroundImage(image4, for: .normal)
        otherclick = "no"
        redioclicked = "yes"
        otherTf.resignFirstResponder()
    }
    
    @IBAction func otheramtBtnClicked(_ sender: Any) {
        parametername = "custom_tip"
        otherclick = "yes"
        
        let imagehome = UIImage(named: "dot.png") as UIImage?
        self.otherBtn.setBackgroundImage(imagehome, for: .normal)
        
        let image1 = UIImage(named: "untickredio.png") as UIImage?
        self.redio5perBtn.setBackgroundImage(image1, for: .normal)
        
        let image2 = UIImage(named: "untickredio.png") as UIImage?
        self.redio10perBtn.setBackgroundImage(image2, for: .normal)
        
        let image3 = UIImage(named: "untickredio.png") as UIImage?
        self.redio20RupeeBtn.setBackgroundImage(image3, for: .normal)
        
        let image4 = UIImage(named: "untickredio.png") as UIImage?
        self.redio50RupeeBtn.setBackgroundImage(image4, for: .normal)
        
        redioclicked = "yes"
        otherTf.isUserInteractionEnabled = true

    }
    
    @IBAction func changesubmitClicked(_ sender: Any) {
        
        otherTf.resignFirstResponder()
        
        if redioclicked == "yes" {
         
        if otherclick == "yes" {
            
            if otherTf.text == "" || otherTf.text == nil {
                
                showSimpleAlert(messagess: "Please enter other amount")
            }else{
            tipvalue = otherTf.text!
                viewhide()
                ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                fisrttimefeeapi()
            }
            
        }else{
           
            viewhide()
            ERProgressHud.sharedInstance.show(withTitle: "Loading...")
            fisrttimefeeapi()
            
        }
        
       
            
        }else{
           
            showSimpleAlert(messagess: "Select atlest one tip option")

            
            }
        
    }
    

}

extension PaymentPage: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return HalfSheetPresentationController(presentedViewController: presented, presenting: presenting)
    }
}

extension PaymentPage {
    func makeCardEntryViewController() -> SQIPCardEntryViewController {
        // Customize the card payment form
        let theme = SQIPTheme()
        theme.errorColor = .red
        theme.tintColor = Color.primaryAction
        theme.keyboardAppearance = .light
        theme.messageColor = Color.descriptionFont
        theme.saveButtonTitle = "Pay"

        return SQIPCardEntryViewController(theme: theme)
    }
}

extension PaymentPage: SQIPCardEntryViewControllerDelegate{
    func cardEntryViewController(_ cardEntryViewController: SQIPCardEntryViewController, didCompleteWith status: SQIPCardEntryCompletionStatus) {
        // Note: If you pushed the card entry form onto an existing navigation controller,
        // use UINavigationController.popViewController(animated:) instead
        dismiss(animated: true) {
            switch status {
            case .canceled:
              //  self.showOrderSheet()
                break
            case .success:
//                guard self.serverHostSet else {
//                   // self.showCurlInformation()
//                    return
//               }

                self.didChargeSuccessfully()
            }
        }
    }

    func cardEntryViewController(_ cardEntryViewController: SQIPCardEntryViewController, didObtain cardDetails: SQIPCardDetails, completionHandler: @escaping (Error?) -> Void) {
        guard serverHostSet else {
            printCurlCommand(nonce: cardDetails.nonce)
            completionHandler(nil)
            return
        }

        ChargeApi.processPayment(cardDetails.nonce) { (transactionID, errorDescription) in
            guard let errorDescription = errorDescription else {
                // No error occured, we successfully charged
                completionHandler(nil)
                return
            }

            // Pass error description
            let error = NSError(domain: "com.example.supercookie", code: 0, userInfo:[NSLocalizedDescriptionKey : errorDescription])
            completionHandler(error)
        }
    }
}

// MARK: - OrderViewControllerDelegate functions
extension PaymentPage: OrderViewControllerDelegate{
    func didRequestPayWithApplyPay() {
        
    }
    
    func didRequestPayWithCard() {
        dismiss(animated: true) {
            let vc = self.makeCardEntryViewController()
            vc.delegate = self

            let nc = UINavigationController(rootViewController: vc)
            self.present(nc, animated: true, completion: nil)
        }
    }



    private func didNotChargeApplePay(_ error: String) {
        // Let user know that the charge was not successful
        let alert = UIAlertController(title: "Your order was not successful",
                                      message: error,
                                      preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    private func didChargeSuccessfully() {
        // Let user know that the charge was successful
//        let alert = UIAlertController(title: "Your order was successful",
//                                      message: "Go to your Square dashbord to see this order reflected in the sales tab.",
//                                      preferredStyle: UIAlertController.Style.alert)
//        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//        present(alert, animated: true, completion: nil)
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")

        orderDetailApi()
        
    }
    
    private func showCurlInformation() {
        let alert = UIAlertController(title: "Nonce generated but not charged",
                                      message: "Check your console for a CURL command to charge the nonce, or replace Constants.Square.CHARGE_SERVER_HOST with your server host.",
                                      preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    private func showMerchantIdNotSet() {
        let alert = UIAlertController(title: "Missing Apple Pay Merchant ID",
                                      message: "To request an Apple Pay nonce, replace Constants.ApplePay.MERCHANT_IDENTIFIER with a Merchant ID.",
                                      preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

